<?php
	function manng_preprocess_html(&$vars){
		drupal_add_library('system', 'ui.dialog');
		drupal_add_js(drupal_get_path('theme','manng').'/js/manng.js','file');
		drupal_add_js(drupal_get_path('theme','manng').'/js/topbar_ng.js','file');
		drupal_add_js(drupal_get_path('theme','manng').'/js/notification_ng.js');
		drupal_add_js(drupal_get_path('theme','manng').'/app/keyboard/jquery.keyboard.js','file');
		// drupal_add_js(drupal_get_path('theme','manng').'/app/storage.sqlite/storage.sqlite.js','file');
		drupal_add_css(drupal_get_path('theme','manng').'/app/icomoon/style.css','file');
		drupal_add_css(drupal_get_path('theme','manng').'/app/keyboard/keyboard.css','file');
	}
	function manng_preprocess_region(&$vars){$theme=alpha_get_theme();if($vars['elements']['#region']=='content')$vars['messages']=$theme->page['messages'];}
	function manng_preprocess_page(&$variables){
		global$language;global$user;
		$user_data=user_load($user->uid);
		$currency_id=isset($user_data->field_currency['und'])?$user_data->field_currency['und'][0]['tid']:'$';
		$currency=i18n_taxonomy_localize_terms(taxonomy_term_load($currency_id));
		$company=array(
			'name'=>isset($user_data->field_company_name_1['und'])?$user_data->field_company_name_1['und'][0]['value']:'',
			'phone'=>isset($user_data->field_company_telephone_1['und'])?$user_data->field_company_telephone_1['und'][0]['value']:'',
			'adress'=>isset($user_data->field_company_address_1['und'])?$user_data->field_company_address_1['und'][0]['value']:'',
		);
		$ProductsNameTitle=isset($user_data->field_products_name_title['und'])?$user_data->field_products_name_title['und'][0]['value']:t('Name');
		drupal_add_js(array(
			'User'=>array(
				'Currency'=>$currency->name,
				'CompanyName'=>$company["name"],
				'CompanyPhone'=>$company["phone"],
				'CompanyAdress'=>$company["adress"],
				'ProductsNameTitle'=>$ProductsNameTitle,
			),
			'Invoice'=>array(
				'path'=>'/'.drupal_get_path('module', 'invoices_ng'),
			),
			'Products_ng'=>array(
				'NewProductRowForm'=>render_new_row_form(),
			),
		),'setting');
		drupal_add_css(path_to_theme().'/css/'.$language->language.'/styles.css',array('group'=>CSS_THEME,'weight'=>99,'preprocess'=>1));
	}
