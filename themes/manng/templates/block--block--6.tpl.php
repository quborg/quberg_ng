<?php $tag = $block->subject ? 'section' : 'div'; ?>
<<?php print $tag; ?><?php print $attributes; ?>>
  <div class="block-inner clearfix">
    <div<?php print $content_attributes; ?>>
      <div class="menu-toolbar-plus">
        <div class="tool-item">
          <a id="active-keyboard" class="icon-keyboard22" <?php print'title="'.t('Virtual Keyboard').'"'; ?>></a>
          <div id="conf-keyboard-ui">
            <div class="div-keyboard">
              <input id="input-keyboard" type="checkbox">
              <span class="span-keyboard"><?php print t('Activate virtual keyboard'); ?></span>
            </div>
            <span class="close-kbui">x</span>
          </div>
        </div>
        <div class="tool-item">
          <a id="export-csv" class="icon-exportfile" <?php print'title="'.t('Export Data to CSV').'"'; ?>></a>
          <div id="export-layout">
            <span class="close-export-csv">x</span>
            <a href="/export-sales.csv"><span class="btn-tool"><?php print t('Sales'); ?></span></a>
            <a href="/export-stock.csv"><span class="btn-tool"><?php print t('Stock'); ?></span></a>
            <a href="/export-invoices.csv"><span class="btn-tool"><?php print t('Invoices'); ?></span></a>
            <a href="/export-contacts.csv"><span class="btn-tool"><?php print t('Contacts'); ?></span></a>
          </div>
        </div>
      </div>
    </div>
  </div>
</<?php print $tag; ?>>