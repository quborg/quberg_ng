<?php?>
<div class="<?php print $classes; ?>">
	<ul id="menu-sales-sidebar" class="menu-sidebar">
		<li id="pointofsale" class="sales-menu-sidebar-link menu-sidebar-link"><a href="#" class="icon-cashregister"><?php print t('Point of sale');?></a></li>
		<li id="salesrevision" class="sales-menu-sidebar-link menu-sidebar-link"><a href="#" class="icon-history"><?php print t('Sales revision');?></a></li>
	</ul>
</div>
