<?php?>
<div class="<?php print $classes;?>">
  <ul id="menu-invoices-sidebar" class="menu-sidebar">
    <li id="invoices-sidebar-sheets" class="invoices-menu-sidebar-link menu-sidebar-link"><a href="#" class="icon-"><i class="icon-files2"></i><?php print t('Invoices');?></a></li>
    <li id="invoice-sidebar-sheets" class="invoices-menu-sidebar-link menu-sidebar-link hide"><a href="#" class="icon-"><i class="icon-file4"></i><?php //print t('Add invoice');?></a></li>
  </ul>
</div>