<?php?>
<div class="<?php print $classes; ?>">
	<ul id="menu-purchases-sidebar" class="menu-sidebar">
		<li id="customers-sidebar-contacts" class="contacts-menu-sidebar-link menu-sidebar-link"><a href="#" class="icon-arrow-bottom-left"><?php print t('Customers');?></a></li>
		<li id="suppliers-sidebar-contacts" class="contacts-menu-sidebar-link menu-sidebar-link"><a href="#" class="icon-arrow-top-right"><?php print t('Suppliers');?></a></li>
	</ul>
</div>