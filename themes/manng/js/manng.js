(function($){
	Drupal.behaviors.ManNg_HeaderLogin={
		attach:function(context,settings){
			jQuery('#block-user').appendTo('#shield-block');
			jQuery('#user-login').click(function(){jQuery('#shield-glass,#shield-block,#block-user').fadeToggle()});
			jQuery('#shield-glass,#close-login').click(function(){jQuery('#shield-glass,#shield-block,#block-user').hide('fade');});
			jQuery('.form-type-password .description a').click(function(){event.preventDefault();});
	}	};
	Drupal.behaviors.ManNg_Time={
		attach:function(context,settings){
			setInterval(function(){var currentTime=new Date(),currentHours=currentTime.getHours(),currentMinutes=currentTime.getMinutes(),currentSeconds=currentTime.getSeconds();currentMinutes=(currentMinutes<10?"0":"")+currentMinutes;currentSeconds=(currentSeconds<10?"0":"")+currentSeconds;var currentTimeString=currentHours+":"+currentMinutes+":"+currentSeconds;$("#time-manng").html(currentTimeString);},1000);
	}	};
	Drupal.behaviors.ManNg_MenuLoadContents={
		attach:function(context,settings){
			jQuery('#content-block-sales')		.append(jQuery('#block-sales-ng-sales-ng'));
			jQuery('#content-block-stock')		.append(jQuery('#block-products-ng-products-ng'));
			jQuery('#content-block-sheets')		.append(jQuery('#block-invoices-ng-invoices-ng-invoices-block'));
			jQuery('#content-block-contacts')	.append(jQuery('#block-contacts-ng-contacts-ng'));
			// jQuery('#content-block-tasks') 		.append(jQuery(''));
			// jQuery('#content-block-agenda') 	.append(jQuery(''));
			jQuery('.index-menu-manng').removeClass('active');
			var Menu={
					sales	  :{
						block:'#content-block-sales'					,
						sidebar:'#block-views-menu-sidebar-menu-sidebar-sales'		,
						messages:'#msg-op-sales'	},
					stock	  :{
						block:'#content-block-stock'					,
						sidebar:'#block-views-menu-sidebar-menu-sidebar-stock'		,
						messages:'#msg-op-stock'	},
					sheets	  :{
						block:'#content-block-sheets'					,
						sidebar:'#block-views-menu-sidebar-menu-sidebar-invoi' 		,
						messages:'#msg-op-sheets'	},
					agenda  :{
						block:'#content-block-tasks'					,
						sidebar:'' 									,
						messages:'#msg-op-agenda'	},
					tasks	  :{
						block:'#content-block-agenda'					,
						sidebar:'' 									,
						messages:'#msg-op-tasks'	},
					contacts:{
						block:'#content-block-contacts'				,
						sidebar:'#block-views-menu-sidebar-menu-sidebar-conta' 		,
						messages:'#msg-op-contacts'}};
			if(context==document)jQuery.each(Menu,function(k,v){jQuery(v.block).add(v.sidebar).hide()});
			jQuery('.menu-ajax-manng').click(function(event){var Lnk=this.getAttribute("id");
				if(!!jQuery(Menu[Lnk].block).html().length){
					event.preventDefault();
					if(!jQuery('#msg-op-'+Lnk).html()&&!(jQuery('#msg-op-'+Lnk).css('display')=='none'))
						jQuery('#region-content').prepend('<div id="msg-op-'+Lnk+'" class="msg-op"></div>');
					else jQuery('#msg-op-'+Lnk).show();
					jQuery.each(Menu,function(k,v){
						if(Lnk==k&&!jQuery('#msg-op-'+k+' .stu').length)jQuery('#msg-op-'+k).append('<div class="stu"></div><div class="err"></div><div class="wrn"></div>');
						if(k!==Lnk){jQuery(v.block).add(v.messages).add(v.sidebar).hide();
							jQuery('#'+k).removeClass('active-color');
						}else if(k!=='home')jQuery('#'+k).addClass('active-color');
						if(!jQuery(v.sidebar+' .menu-sidebar-link').hasClass('active-vmsl')){jQuery(v.sidebar+' .menu-sidebar-link:first').click();}});
					jQuery(Menu[Lnk].block).add(Menu[Lnk].sidebar).show();
				}
				else window.location.replace('/');
			});
			if(window.location.pathname=='/')Api_LoadData_ProductsNames();
	}	};

	Drupal.behaviors.ManNg_ScrollBar={
		attach:function(context,settings){
			jQuery('#invoice-content').height(jQuery(window).height()-173);
			jQuery('#products-table-wrapper,#sales-table,#block-invoices-content,#user-profile-form,#salesrevision-content-table').height(jQuery(window).height()-120);
	}	};


})(jQuery);
