(function($){
	msgStu='<div class="messages status"><ul>'+Drupal.t('Action completed successfully.')+'</ul></div>';
	msgWrn='<div class="messages warning"><ul>'+Drupal.t('Error or Service unavailable, please try to resolving or refresh this page.')+'</ul></div>';
	msgErr='<div class="messages error"><ul>'+Drupal.t('Please resolve errors <em>notifications</em>.')+'</ul></div>';
	Drupal.behaviors.NotificationsNg={
		attach:function(context,settings){
			NotifyNg=function(type,module,id,dateNotify){dateNotify=dateNotify||false;id=id||false;
				ClearNotifications(module,id);
				var MSG={ 'stu':msgStu, 'wrn':msgWrn, 'err':msgErr, };
				jQuery('#msg-op-'+module+' .'+type).html(MSG[type]).find('.messages').append('<span class="close stu">x</span>');
				setTimeout(function(){jQuery('#msg-op-'+module+' .'+type+' .messages').fadeOut('slow');},5000);
				jQuery('.msg-op .messages .close').click(function(){jQuery(this).parent().fadeOut('slow')});
				if(id&&type=='err'&&module=='stock'){ProductErroInfo(id);}
				if(id&&type=='err'&&module=='sales'){SaleErroInfo(id);DateErrorInfo();} }
			ProductErroInfo=function(id){
				var ul='<ul>',input='.row-id-'+id+' .pp-holder input',select='.select-list-'+id+' select';
				if(jQuery(input).hasClass('empty'))ul+='<li>'+Drupal.t('Title field required.')+'</li>';
				if(jQuery(input).hasClass('negatif'))ul+='<li>'+Drupal.t('Price and Quantity must be positif.')+'</li>';
				if(jQuery(input).hasClass('not-numeric'))ul+='<li>'+Drupal.t('Price and Quantity fields must be numerique.')+'</li>';
				if(jQuery(select).hasClass('select-error'))ul+='<li>'+Drupal.t('Please select a category for this product')+'</li>';
				ul+='</ul>';
				jQuery('.product-notifications.'+id).html(ul);
				jQuery('.icon-info.'+id).removeClass('hide-info'); }
			SaleErroInfo=function(id){
				var ul='<ul>',input='.sale-row.sr-'+id+' .main-form';
				if(jQuery(input).hasClass('empty'))ul+='<li>'+Drupal.t('Title field required.')+'</li>';
				if(jQuery(input).hasClass('negatif'))ul+='<li>'+Drupal.t('Price and Quantity must be positif.')+'</li>';
				if(jQuery(input).hasClass('invalid'))ul+='<li>'+Drupal.t('Product name not found.')+'</li>';
				if(jQuery(input).hasClass('quantity'))ul+='<li>'+Drupal.t('Quantity must be greater than 0.')+'</li>';
				if(jQuery(input).hasClass('unchosen'))ul+='<li>'+Drupal.t('Choose product from autocomplete list.')+'</li>';
				if(jQuery(input).hasClass('overstock'))ul+='<li>'+Drupal.t('Quantity greater than available stock.')+'</li>';
				if(jQuery(input).hasClass('not-numeric'))ul+='<li>'+Drupal.t('Price and Quantity fields must be numerique.')+'</li>';
				ul+='</ul>';
				jQuery('.sales-notifications.'+id).html(ul);
				jQuery('#imsg-'+id).addClass('red-info'); }
			DateErrorInfo=function(){if(jQuery('#date-sales-datepicker-popup-0').hasClass('error-date'))jQuery('#date-sales').addClass('show-description');}
			ClearNotifications=function(module,id){jQuery('#msg-op-'+module+' .err').add('#msg-op-'+module+' .wrn').add('#msg-op-'+module+' .stu').fadeIn('slow').html('');
				if(module=="sales")ClearSalesNotification(id);
				if(module=="stock"){ClearProductNotification(id);ClearDateNotification();} }
			ClearSalesNotification=function(id){jQuery('#imsg-'+id+' div').html('').parent().removeClass('red-info');}
			ClearProductNotification=function(id){jQuery('.icon-info.'+id+' div').html('').parent().addClass('hide-info');}
			ClearDateNotification=function(){jQuery('#date-sales').removeClass('show-description');}
		}
	}

})(jQuery);
