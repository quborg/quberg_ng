<div<?php print $attributes; ?>>
  <div<?php print $content_attributes; ?>>
    <?php if ($user_logo || $site_name): ?>
      <div id="home-logo" class="branding-data clearfix menu-ajax-manng">
        <?php if ($user_logo): ?>
          <div class="logo-img">
            <?php print theme('image_style', array('path' => $user_logo, 'style_name' => 'logo')); ?>
          </div>
        <?php elseif ($site_name): ?>
          <hgroup class="site-name-slogan">        
            <h1 class="site-name"><?php print $linked_site_name; ?></h1>
          </hgroup>
        <?php endif; ?>
      </div>
    <?php endif; ?>
    <?php print $content; ?>
  </div>
</div>