<?php $tag = $block->subject ? 'section' : 'div'; ?>
<<?php print $tag; ?><?php print $attributes; ?>>
	<div class="block-inner clearfix">
		<div<?php print $content_attributes; ?>>
			<a href="/user/<?php print $user->uid;?>/edit" data-thmr="thmr_9"><div class="icon-folder-open" id="account" title="<?php print t('Account'); ?>"></div></a>
			<a href="/user/logout" data-thmr="thmr_11"><div class="icon-exit" id="logout" title="<?php print t('Log out'); ?>"></div></a>
		</div>
	</div>
</<?php print $tag; ?>>