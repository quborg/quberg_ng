<div<?php print $attributes; ?>>
    <?php if ($messages): ?>
        <div id="messages" class="grid-9"><?php print $messages; ?></div>
    <?php endif; ?>
    <div<?php print $content_attributes; ?>>
        <a id="main-content"></a>
        <?php print render($title_prefix); ?>
        <?php if ($title): ?>
        <?php if ($title_hidden): ?><div class="element-invisible"><?php endif; ?>
        <h1 class="title" id="page-title"><?php print $title; ?></h1>
        <?php if ($title_hidden): ?></div><?php endif; ?>
        <?php endif; ?>
        <?php print render($title_suffix); ?>
        <?php if(isset($user->roles[3])): ?>
            <?php if($user->roles[3]=='administrator'): ?>
                <?php if ($tabs && !empty($tabs['#primary'])): ?><div class="tabs clearfix"><?php print render($tabs); ?></div><?php endif; ?>
            <?php endif; ?>
        <?php endif; ?>
        <?php if ($action_links): ?><ul class="action-links"><?php print render($action_links); ?></ul><?php endif; ?>
        <div id="content-block-sales"></div>
        <div id="content-block-stock"></div>
        <div id="content-block-sheets"></div>
        <div id="content-block-tasks"></div>
        <div id="content-block-agenda"></div>
        <div id="content-block-contacts"></div>
        <div id="content-block-data">
            <div id="cbd-products-names"></div>
            <div id="cbd-sales-deleted"></div>
            <div id="cbd-inv-stk-deleted"></div>
            <div id="cbd-customers-names"></div>
        </div>
        <?php print $content; ?>
        <?php if ($feed_icons): ?><div class="feed-icon clearfix"><?php print $feed_icons; ?></div><?php endif; ?>
    </div>
</div>