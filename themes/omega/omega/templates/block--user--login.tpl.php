<?php $tag = $block->subject ? 'section' : 'div'; ?>
<<?php print $tag; ?><?php print $attributes; ?>>
  <div class="block-inner clearfix">
    <div id="user-login" class="icon-enter"></div>
    <div id="block-user">
      <span id="close-login">x</span>
      <div<?php print $content_attributes; ?>>
        <?php print $content ?>
      </div>
    </div>
  </div>
</<?php print $tag; ?>>