(function($){

	Api_InvoiceAutoComplete=function(i,value,orizon,key){
		jQuery.ajax({url:'invoices_ng/autocomplete/'+value,
			beforeSend:function(){jQuery('.invoice-des.'+i).css('background-position',orizon+' -16px');},
			success:function(data){Structure_BuildInvoiceProductsList(i,data,key);},
			complete:function(){jQuery('.invoice-des.'+i).css('background-position',orizon+' 4px');},
			error:function(){console.log('--');NotifyNg('wrn','sheets')},
	});	}

	Api_CustomerAutoComplete=function(value,orizon,key,el,ul){
		jQuery.ajax({url:'invoices_ng/customers/'+value,
			beforeSend:function(){jQuery(el).css('background-position',orizon+' -16px');},
			success:function(data){Structure_BuildInvoiceCustomerList(data,key,el,ul);},
			complete:function(){jQuery(el).css('background-position',orizon+' 4px');},
			error:function(){console.log('--');NotifyNg('wrn','sheets')},
	});	}

	Api_InvoiceSave=function(id){
		jQuery.ajax({url:'invoices_ng/save_invoice/'+id,type:'POST',data:Structure_SerializeInvoice(),
			beforeSend:function(){jQuery('#wait-action-load').addClass('show');},
			success:function(data){
				if(parseInt(jQuery(data).filter('invoiceid').text())){
					jQuery('#invoice-id').val(jQuery(data).filter('invoiceid').text());
					NotifyNg('stu','sheets');
				}else{console.log('--');NotifyNg('wrn','sheets');}},
			complete:function(){jQuery('#wait-action-load').removeClass('show');},
			error:function(){console.log('--');NotifyNg('wrn','sheets')},
	});	}

	Api_LoadData_CustomersNames=function(){
	jQuery.ajax({url:'customers_ng/customers_names',
		success:function(data){if(data!=='wrn')jQuery('#content-block-data #cbd-customers-names').html(data);else NotifyNg('wrn','contacts');},
		error:function(){NotifyNg('wrn','contacts');},}); }

})(jQuery);
