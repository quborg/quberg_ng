(function($){

	Drupal.behaviors.InvoiceNg={
		attach:function(context,settings){
			jQuery('#add-invoice').live('click',function(){
				jQuery('#sheets-block').hide();
				jQuery('#invoice-content').show();
				jQuery('#print-sheets').removeClass('disable');
				jQuery('#invoice-content').on('keyup click change',function(){
					jQuery('#save-sheets').removeClass('disable');
				})
			})
			jQuery('#close-invoice-sheet').live('click',function(){
				jQuery('#sheets-block').show();
				jQuery('#invoice-content').hide();
				jQuery('#print-sheets').addClass('disable');
				jQuery('#save-sheets').addClass('disable');
			})
	}	};


})(jQuery);
