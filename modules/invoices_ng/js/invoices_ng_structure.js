(function($){

	Structure_InvoiceSaveProcess=function(){
		jQuery('#save-sheets').addClass('disable');
		var id=jQuery('#invoice-id').val()?jQuery('#invoice-id').val():'new';
		Structure_validateInvoice();
		if(jQuery('#invoice-content .tocheck').hasClass('error')){NotifyNg('err','sheets');return false;}
		Api_InvoiceSave(id);
		// if(jQuery('.invoice-type-select option[value="'+jQuery('.invoice-type-select').val()+'"]').html()=="أمر") UPDATE STOCK
	}

	Structure_SerializeInvoice=function(){var data='',j=0,
		customer=jQuery('#customer-name').attr('data-ref')+'::'+jQuery('#customer-name').val(),
		date=jQuery('#invoice-date').val().replace(/\//g,'-'),
		type=jQuery('.invoice-type-select').val(),
		status=jQuery('.invoice-status-select').val(),
		n_items=jQuery('#invoice-table tbody tr').length,
		subtotal=jQuery('#subtotal').html(),
		downpayment=jQuery('#downpayment').val(),
		amount=jQuery('#amount').html(),
		alphabetic=jQuery('#alphabetic-amount').html();
		data='customer='+customer+'&type='+type+'&status='+status+'&date='+date+'&subtotal='+subtotal+'&downpayment='+downpayment+'&amount='+amount+'&alphabetic='+alphabetic;
		data+='&n_items'+n_items;
		jQuery('#invoice-table tbody tr').each(function(){var id=jQuery(this).attr('data-id');
			if(!!jQuery('#inv-des-'+id).attr('data-ref')){
				data+='&des'+j+'='+jQuery('#inv-des-'+id).attr('data-ref')+'::'+jQuery('#inv-des-'+id).val();
				data+='&qty'+j+'='+jQuery('#inv-qty-'+id).val();
				data+='&pxu'+j+'='+jQuery('#inv-pxu-'+id).val();
				j++;
			}
		})
		return data;
	}

	Structure_validateInvoice=function(){
		var date='#invoice-date',data_prods=jQuery('#cbd-products-names').html(),data_custos=jQuery('#cbd-customers-names').html(),reg=/^(0?[1-9]|[12][0-9]|3[01])[\/](0?[1-9]|1[012])[\/]\d{4}$/;
		jQuery('.tocheck').removeClass('error empty unchosen invalid negatif not-numeric error-date');
		if(!reg.test(jQuery(date).val()))jQuery(date).addClass('error error-date');
		if(jQuery('#customer-name').val()=='')jQuery('#customer-name').addClass('error empty');
		if(!jQuery('#customer-name').attr('data-ref')){jQuery('#customer-name').addClass('error unchosen');}
		if(jQuery('#customer-name').val()!==''&&!!jQuery('#customer-name').attr('data-ref')&&!!data_custos.length){var valid=false;
			jQuery.each(data_custos.split('::'),function(k,v){if(v==jQuery('#customer-name').val()){valid=true;return false;}});
			if(!valid)jQuery('#customer-name').addClass('error invalid');}
		jQuery('#invoice-table tbody tr').each(function(){var id=jQuery(this).attr('data-id');
			if(!jQuery('#inv-des-'+id).attr('data-ref')&&jQuery('#inv-des-'+id).val().replace(/\ /g,'')!==''){jQuery('#inv-des-'+id).addClass('error unchosen');}
			if(jQuery('#inv-des-'+id).val()!==''&&!!jQuery('#inv-des-'+id).attr('data-ref')&&!!data_prods.length){var valid=false;
				jQuery.each(data_prods.split('::'),function(k,v){if(v==jQuery('#inv-des-'+id).val()){valid=true;return false;}});
				if(!valid)jQuery('#inv-des-'+id).addClass('error invalid');}
			if(!jQuery.isNumeric(jQuery('#inv-qty-'+id).val()))jQuery('#inv-qty-'+id).addClass('error not-numeric');
			if(!jQuery.isNumeric(jQuery('#inv-pxu-'+id).val()))jQuery('#inv-pxu-'+id).addClass('error not-numeric');
			if(jQuery('#inv-qty-'+id).val()<0)jQuery('#inv-qty-'+id).addClass('error negatif');
			if(jQuery('#inv-pxu-'+id).val()<0)jQuery('#inv-pxu-'+id).addClass('error negatif');})
		if(!jQuery.isNumeric(jQuery('#downpayment').val()))jQuery('#downpayment').addClass('error not-numeric');
	}

	Structure_BuildInvoiceProductsList=function(i,data,key){var li='';
		for(var j=0;j<data.length;j++){var ram=data[j].split('::');li+='<li data-data="'+data[j]+'">'+ram[1]+'</li>';}
		if(li!==''&&jQuery('.invoice-des.'+i).hasClass('autoc-actif')&&key==window.InvoiceAutoComplete){
			Structure_ClearInvoiceAutocompleteList(i);
			jQuery('.invoice-autocomplete.'+i).append(li).addClass('show').css('width',jQuery('.invoice-des').width()-10);}
		jQuery('ul.invoice-autocomplete li').click(function(){var ram=jQuery(this).attr('data-data').split('::');
			jQuery('#inv-des-'+i).removeClass('autoc-actif').val(ram[1]).attr('data-ref',ram[0]).parent().attr('data-content',ram[3]+' '+Drupal.t('unit'));
			jQuery('#inv-pxu-'+i).val(ram[2]);
			Structure_ClearInvoiceAutocompleteList(i);
			jQuery('#invoice-table').click();	})}

	Structure_ClearInvoiceAutocompleteList=function(id){jQuery('.invoice-autocomplete.'+id).html('').removeClass('show');}

	Structure_BuildInvoiceCustomerList=function(data,key,el,ul){var li='';console.log(el+':'+ul);
		for(var j=0;j<data.length;j++){var ram=data[j].split('::');li+='<li data-data="'+data[j]+'">'+ram[1]+'</li>';}
		if(li!==''&&jQuery(el).hasClass('autoc-actif')&&key==window.CustomerAutoComplete){
			Structure_ClearCustomerAutocompleteList(ul);
			jQuery(ul).append(li).addClass('show').css('width',jQuery(el).width()-8);}
		jQuery(ul+' li').click(function(){var ram=jQuery(this).attr('data-data').split('::');
			jQuery(el).removeClass('autoc-actif').val(ram[1]).attr('data-ref',ram[0]);
			jQuery('span.customer-name').html(ram[1]);
			Structure_ClearCustomerAutocompleteList(ul); })}

	Structure_ClearCustomerAutocompleteList=function(ul){jQuery(ul).html('').removeClass('show');}


})(jQuery);
