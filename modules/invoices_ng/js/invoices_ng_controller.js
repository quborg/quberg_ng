(function($){

	Drupal.behaviors.InvoicesNg_Sidebar_Controller={ // SWITCH SIDEBAR MENU STOCK BLOCK
		attach:function(context,settings){
			var Menu={'invoice-sidebar-sheets':'#add-invoice',
								'invoices-sidebar-sheets':'#block-invoices',};
			jQuery('.invoices-menu-sidebar-link').click(function(){var Lnk=this.getAttribute("id");
				jQuery.each(Menu,function(k,v){
					if(k!==Lnk){jQuery(v).hide();jQuery('#'+k).removeClass('active-vmsl');ClearNotifications('sheets');}
					else{jQuery(v).show();jQuery('#'+k).addClass('active-vmsl');}
				});
			});
	}	};

	Drupal.behaviors.InvoicesNg_Actions={ // INVOICE ACTIONS & BUTTONS TOOL
		attach:function(context,settings){
			jQuery('#add-new-invoice').live('click',function(){
				jQuery('#print-sheets,#save-sheets').removeClass('disable');
				jQuery('#block-invoices').hide();jQuery('#add-invoice').show();
				jQuery('#invoices-sidebar-sheets').removeClass('active-vmsl');
				jQuery('#invoice-sidebar-sheets').addClass('active-vmsl');
			});
			jQuery('#close-invoice,#invoices-sidebar-sheets').live('click',function(){
				jQuery('#print-sheets,#save-sheets').addClass('disable');
				jQuery('#add-invoice').hide();jQuery('#block-invoices').show();
				jQuery('#invoices-sidebar-sheets').addClass('active-vmsl');
				jQuery('#invoice-sidebar-sheets').removeClass('active-vmsl');
			});
			jQuery('#add-invoice').live('change input keyup',function(){jQuery('#save-sheets').removeClass('disable');})
	}	};

	Drupal.behaviors.InvoicesNg_FilterDate={ // INVOICES FILTER DATE
		attach:function(context,settings){
			if(jQuery('#invoice-filter-date').length){jQuery('#invoice-filter-date').datepicker({dateFormat:'dd/mm/yy'});}
	}	};

	Drupal.behaviors.InvoicesNg_FilterCustomer={ // INVOICES FILTER CUSTOMER
		attach:function(context,settings){window.CustomerAutoComplete=0;
			jQuery('#invoice-filter-customer').live('keyup',function(){var timer=0,value=jQuery(this).val(),orizon=jQuery('body').hasClass('i18n-ar')?'2px':'100%',el='#invoice-filter-customer',ul='#invoice-filter-customer-autocomplete';
				jQuery(this).addClass('autoc-actif'); jQuery(this).attr('data-ref','');
				if(jQuery(this).val()&&jQuery(this).val().replace(/\ /g,'')!==''){
					clearTimeout(timer);
					timer=setTimeout(function(){
						window.CustomerAutoComplete=window.CustomerAutoComplete==i++?i:++i;
						Api_CustomerAutoComplete(value,orizon,window.CustomerAutoComplete,el,ul);
					},300);
				}else{jQuery(this).removeClass('autoc-actif').css('background-position',orizon+' 4px');Structure_ClearCustomerAutocompleteList(ul);}
			});
	}	};

})(jQuery);
