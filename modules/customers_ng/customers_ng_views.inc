<?php

function views_invoices_filters(){
  return 'filters : html + invoices_ng_customers("all"); ';
}

function views_invoices_head(){
  return 'head : html div table';
}

function views_invoices_body(){
  return 'body : html + invoices_ng_invoices("all","all","return") as rows div body';
}