(function($){

	Drupal.behaviors.SalesNG_GetNewSaleRow_Controller={ // ADD NEW ROW SALE FORM
		attach:function(context,settings){
			jQuery('#validate-sale-row').live('click',function(){
				var adn=jQuery('#table-sales-rows tbody tr').length,id=parseInt(jQuery(this).parent().parent().attr('data-id'));
				if(Structure_ValidateSaleFields(id)){Structure_GetNewSaleRow((adn+1));Structure_SaveSalesDateProcess();}
			});
	}	};

	Drupal.behaviors.SalesNG_AutoCompleteLineSale_Controller={ // AUTOCOMPLETE
		attach:function(context,settings){var timer,i=0;window.SaleAutoComplete=0;
			jQuery('.des-autocomplete').live('keyup',function(){ // LIVE DESIGNATION AUTOCOMPLETE
				var value=jQuery(this).val(),id=jQuery(this).attr('data-id'),orizon=jQuery('body').hasClass('i18n-ar')?'2px':'100%';
				Structure_SaleResetDataAndIInfoStatus(id);
				if(jQuery(this).val()&&jQuery(this).val().replace(/\ /g,'')!==''){
					clearTimeout(timer);
					timer=setTimeout(function(){
						window.SaleAutoComplete=window.SaleAutoComplete==i++?i:++i;
						Api_SalesAutoCompleteDesignation(id,value,orizon,window.SaleAutoComplete);
					},400);
				}else{jQuery('#des-'+id).removeClass('autoc-actif').css('background-position',orizon+' 4px');Structure_StockClearDesignationAutocompleteList();}
			});
	}	};

	Drupal.behaviors.SalesNG_LoadDateSales_Controller={ // 1ST LOAD FOR TODAY SALES + LOAD SALES ON EACH SELECTED DATE + REFRESH SALES
		attach:function(context,settings){
			jQuery(document).ready(jQuery('#sales').one('click',function(){Api_LoadDateSales()})); // 1ST LOAD FOR TODAY SALES
			jQuery('#refresh-sales').live('click',function(){Api_LoadDateSales()})
			jQuery('#date-sales input').keydown(function(e){e.preventDefault();}).bind('change',function(e){ // LOAD SALES ON EACH SELECTED DATE
				jQuery('#cbd-sales-deleted').html('');
				var adn=jQuery('#table-sales-rows tbody tr').length,save=false;
				if(adn>1){
					for(var i=1;i<adn;i++)if(jQuery('.op-column.op-row-'+i+' i.ii.iinfo').hasClass('iinfo-unstatus'))save=true;
					if(save)Structure_SalesSwitchDateConfirmation(Drupal.t('Confirme to save '));
				}
				if(!save){Structure_ClearSelectToolCheckbox();Api_LoadDateSales();}
			})
	}	};

	Drupal.behaviors.SalesNg_Sidebar_Controller={ // SALES SIDEBAR MENU CONTROLLER BLOCK SWITCHER
		attach:function(context,settings){
			var Menu={	'newsales':'#block-sales-ng-sales-ng',
									'allsales':'', };
			if(context==document)jQuery.each(Menu,function(k,v){jQuery(v).hide();});
			jQuery('.sales-menu-sidebar-link').click(function(){var Lnk=this.getAttribute("id");
				jQuery.each(Menu,function(k,v){
					if(k!==Lnk){jQuery(v).hide();jQuery('#'+k).removeClass('active-vmsl');}
					else{jQuery('#'+k).addClass('active-vmsl');jQuery(v).show();}
				});
			})
	}	};

	Drupal.behaviors.SalesNg_Total_Controller={ // TOTAL SALE ROW CALCULATOR
		attach:function(context,settings){
			jQuery('#table-sales-rows .sale-row').live('keyup change input keypress keydown',function(){
				var id=jQuery(this).attr('data-id');
				jQuery('#tot-'+id).val(eval(jQuery('#qty-'+id).val()*jQuery('#pxu-'+id).val()).toFixed(2));
			})
	}	};

	Drupal.behaviors.SalesNg_SelectTool_Controller={ // CHECKBOXS SELECT TOOL STATUS
		attach:function(context,settings){
			if(context==document)jQuery('#sales-select-tool-btn').click(function(event){
				if(!jQuery('#sales-select-tool-content').is(':visible')){jQuery('#sales-select-tool-content').show(100); return false;} 
				else jQuery('#sales-select-tool-content').hide(100); })
			jQuery(document).click(function(){if(jQuery('#sales-select-tool-content').css('display')=="block")jQuery('#sales-select-tool-content').hide(100);});
			jQuery('#icheckbox').click(function(){
				switch (jQuery(this).attr('data-state')){
					case 'none' : jQuery(this).attr('data-state','full'); jQuery('i.icheck input').prop('checked',true);  break;
					case 'full' : jQuery(this).attr('data-state','none'); jQuery('i.icheck input').prop('checked',false); break;
					case 'part' : jQuery(this).attr('data-state','none'); jQuery('i.icheck input').prop('checked',false); break;
				}Structure_Icheckbox(); })
			jQuery('i.icheck input').live('click',function(){Structure_CheckboxState();Structure_Icheckbox();})
			jQuery('#sales-select-tool-content li').click(function(){var li=this.getAttribute("id");Structure_ListSelect(li);})
	}	};

	Drupal.behaviors.SalesNg_DeleteSalesLines_Controller={ // DELETE SALES LINES
		attach:function(context,settings){
			jQuery('i.icheck input,#icheckbox,#sales-select-tool-content li').live('click change',function(){ // ENABLE DELETE BOUTTON IF SALE LINE CKECKED ONE
				if(jQuery('i.icheck input:checked').length)jQuery('#delete-sales').removeClass('disable');
				else jQuery('#delete-sales').addClass('disable');})
			jQuery('#delete-sales').click(function(){					 // DELETE PROCESS
				if(!jQuery(this).hasClass('disable')){jQuery(this).addClass('disable');Structure_ClearSelectToolCheckbox();
					Structure_SalesRemoveSelectedLines();	}
				if(jQuery('#inode').val()!=='new')Api_SaveSalesDate(jQuery('#inode').val());
			})
	}	};

	Drupal.behaviors.SalesNG_SaveSales_Controller={ // SAVING SALES DATE PROCESS
		attach:function(context,settings){
			jQuery('#menu-toolbar-sales #save-sales').live('click',function(){var adn=jQuery('#table-sales-rows tbody tr').length;
				if(!jQuery(this).hasClass('disable'))
					if(!!jQuery('#ref-'+adn).val()&&adn>1)Structure_SavableRowConfirmation(adn);
					else Structure_SaveSalesDateProcess();
			});
	}	};

	Drupal.behaviors.SalesNG_SalesStockFrontEndSystem_Controller={ // SALES STOCK FRONT-END SYSTEM
		attach:function(context,settings){
			jQuery('input[id^="qty-"]').live('input',function(){
				var id=parseInt(jQuery(this).attr('data-id')),
						adn=jQuery('#table-sales-rows tbody tr').length,
						ref=jQuery('#ref-'+id).val(),
						stock=parseInt(jQuery('#mid-'+id).attr('data-load-stock'))-parseInt(jQuery(this).val());
						jQuery('#des-'+id).parent().attr({'data-content':stock+' '+Drupal.t('unit'),'data-stock':stock});
						jQuery('#qty-'+id).attr('data-stock',stock);
				for(var i=1;i<=adn;i++){
					if(ref==jQuery('#ref-'+i).val()&&i!==id){
						jQuery('#qty-'+i).attr('data-stock',stock);
						jQuery('#des-'+i).parent().attr({'data-content':stock+' '+Drupal.t('unit'),'data-stock':stock});
						jQuery('#mid-'+i).attr('data-load-stock',parseInt(jQuery('#qty-'+i).attr('data-stock'))+(!!parseInt(jQuery('#qty-'+i).val())?parseInt(jQuery('#qty-'+i).val()):0));
					}
				}
				if(jQuery('#sales-autocomplete-list').css('display')=='block'){
					jQuery('#sales-autocomplete-list li').each(function(){
						if(ref==jQuery(this).attr('data-ref')){
							var ram=jQuery(this).attr('data-desv').split('::');
							jQuery(this).attr('data-desv',ram[0]+'::'+ram[1]+'::'+stock+'::'+ram[3]);
							jQuery(this).html('<span class="name">'+ram[1]+'</span><span class="autocomplete-product-details">'+stock+' '+Drupal.t('unit')+', '+ram[3]+' '+Drupal.t(Drupal.settings.User.Currency)+'</span>');
						}
					})
				}
			})
	}	};

})(jQuery);