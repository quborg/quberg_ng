(function($){

	Structure_GetNewSaleRow=function(id){ // PREPEND NEW SALE ROW FORM
		var tr='<tr class="sale-row sr-'+id+'" data-id="'+id+'"><td class="select"><div class="op-column op-row-'+id+'"><input id="mid-'+id+'" name="mid-'+id+'" value="'+jQuery.now()+'" type="hidden"><i class="ii icheck iinfo iinfo-unstatus" data-id="'+id+'"><div class="fake-checkbox"></div></i><i id="imsg-'+id+'" class="ii imsg icon-info"><div class="sales-notifications i-notifications '+id+'"></div></i></div></td><td class="add-sale"><i id="validate-sale-row" class="icon-checkmark-circle '+id+'" data-id="'+id+'"></i></td><td class="col-name" width="30%" class="des"><input type="hidden" id="ref-'+id+'"><input type="hidden" id="desv-'+id+'"><input type="text" id="des-'+id+'" name="des-'+id+'" class="main-form form-text form-autocomplete des-autocomplete" autocomplete="off" data-id="'+id+'"></td><td class="col-qty" width="10%"><input class="main-form form-text" type="number" id="qty-'+id+'" name="qty-'+id+'" data-id="'+id+'" min="0"></td><td class="col-pxu" width="10%"><input class="main-form form-text" type="number" id="pxu-'+id+'" name="pxu-'+id+'" data-id="'+id+'" min="0"></td><td class="col-tot" width="10%"><input type="number" id="tot-'+id+'" name="tot-'+id+'" disabled="disabled"></td><td><textarea class="main-form form-text" id="rem-'+id+'" name="rem-'+id+'" rows="1" data-id="'+id+'"></textarea></td></tr>';
		jQuery('#validate-sale-row.'+(id-1)).remove();jQuery('.op-row-'+(id-1)+' i.ii.iinfo').html('<input type="checkbox" class="icheck-'+(id-1)+'">');
		jQuery('#table-sales-rows tbody').append(tr); 
		jQuery('#sales-table').scrollTop(jQuery('#sales-table')[0].scrollHeight); }

	Structure_GetSalesRows=function(iurl,method,adn,fill,xDATA){ //  A DATE SALES
		jQuery.ajax({url:iurl,
			beforeSend:function(){jQuery('#wait-action-load').addClass('show2');},
			success:function(data){if(method=='html'){jQuery('#table-sales-rows tbody').html(jQuery(data).find('#sale-row tbody').html());}},
			error:function(){console.log('--');NotifyNg('wrn','sales');},
			complete:function(){
				for(var i=1;i<(adn+1);i++)
					if(jQuery('.sr-'+i).length>1)
						jQuery('.sr-'+1+':gt(0)').remove();
				if(fill){
					jQuery('#inode').val(xDATA[0]['inode']);
					jQuery('#date-sales-datepicker-popup-0').val(xDATA[0]['idate']);
					console.log(xDATA);
					for(var i=1;i<parseInt(xDATA[0]['nrows'])+1;i++){
						var row=xDATA[1][(i-1)];
						jQuery('.icheck-'+i).parent().removeClass('iinfo-unstatus').addClass('iinfo-status');
						jQuery('#mid-'+i).val(row['mid']).attr('data-load-stock',parseInt(row['stk'])+parseInt(row['qty']));
						jQuery('#ref-'+i).val(row['nid']); // PRODUCT ID
						jQuery('#des-'+i).val(row['des']).parent().attr({'data-content':row['stk']+' '+Drupal.t('unit'),'data-stock':row['stk'],'data-loaded-ref':row['nid'],'data-loaded-qty':row['qty']});
						jQuery('#qty-'+i).val(row['qty']).attr('data-stock',row['stk']);
						jQuery('#pxu-'+i).val(row['pxu']);
						jQuery('#rem-'+i).val(row['rem']);
						Structure_SaleInfo(i); }
					jQuery('.sale-row').change(); // TRIGGER FOR CALCULATE TOTAL
					Structure_GetNewSaleRow((jQuery('#table-sales-rows tbody tr').length+1));
				}else Structure_SaleInfo((adn+1));
				if(!jQuery('#sales-autocomplete-list').length)jQuery('<ul id="sales-autocomplete-list" class="autocomplete-list"></ul>').insertBefore('#table-sales-rows');
				jQuery('#menu-toolbar-sales #save-sales').addClass('disable');
				ClearNotifications('sales');
				jQuery('#wait-action-load').removeClass('show2'); } })}

	Structure_SaleResetDataAndIInfoStatus=function(i){
		jQuery('#ref-'+i).val('');jQuery('#des-'+i).addClass('autoc-actif').parent().attr({'data-content':'','data-stock':''});jQuery('#pxu-'+i).val('');jQuery('#qty-'+i).attr('data-stock','');jQuery('#mid-'+i).attr('data-load-stock','');
		jQuery('.icheck-'+i).parent().removeClass('iinfo-status iinfo-error').addClass('iinfo-unstatus');	}

	Structure_BuildSalesProductsEntityList=function(i,data,key){var li='';
		for(var j=0;j<data.length;j++){
			var ram=data[j].split('::');
			jQuery('.sale-row').each(function(){var id=jQuery(this).attr('data-id');
				if(parseInt(jQuery(this).find('#ref-'+id).val())==parseInt(ram[0])){
					ram[2]=parseInt(jQuery(this).find('td.des').attr('data-stock'));
					data[j]=ram[0]+'::'+ram[1]+'::'+ram[2]+'::'+ram[3];
					return false;
				} })
			li+='<li id="li-prod-'+j+'" data-id="'+j+'" data-ref="'+ram[0]+'" data-desv="'+data[j]+'"><span class="name">'+ram[1]+'</span> <span class="autocomplete-product-details">'+ram[2]+' '+Drupal.t('unit')+', '+ram[3]+' '+Drupal.t(Drupal.settings.User.Currency)+'</span>'+'</li>'; }
		if(li!==''&&jQuery('#des-'+i).hasClass('autoc-actif')&&key==window.SaleAutoComplete){Structure_StockClearDesignationAutocompleteList();jQuery('ul#sales-autocomplete-list').append(li).addClass('show').css('width',jQuery('#des-'+i).width()-8).insertAfter(jQuery('#des-'+i+'.autoc-actif'));}
		jQuery('ul#sales-autocomplete-list li').click(function(){var designation=jQuery(this).find('span.name').text();
			jQuery('#des-'+i).removeClass('autoc-actif').val(designation);jQuery('#desv-'+i).val(jQuery(this).attr('data-desv'));
			Structure_SaleInfo(i); })	}

	Structure_SaleInfo=function(i){
		var ram=jQuery('#desv-'+i).val().split('::');
		if(ram.length>1){
			if(jQuery('.sale-row.sr-'+i+' td.des').attr('data-loaded-ref')==ram[0])
				ram[2]-=parseInt(jQuery('#qty-'+i).val())-parseInt(jQuery('.sale-row.sr-'+i+' td.des').attr('data-loaded-qty'));
			else if(!!jQuery('#qty-'+i).val()){
				ram[2]=parseInt(ram[2])-parseInt(jQuery('#qty-'+i).val());
				var adn=jQuery('#table-sales-rows tbody tr').length;
				for(var j=1;j<=adn;j++){
					if(ram[0]==jQuery('#ref-'+j).val()&&j!==i){
						jQuery('#qty-'+j).attr('data-stock',ram[2]);
						jQuery('#des-'+j).parent().attr({'data-content':ram[2]+' '+Drupal.t('unit'),'data-stock':ram[2]});
						jQuery('#mid-'+j).attr('data-load-stock',parseInt(ram[2])+(!!parseInt(jQuery('#qty-'+j).val())?parseInt(jQuery('#qty-'+j).val()):0));
					}
				}
			}
			jQuery('#mid-'+i).attr('data-load-stock',parseInt(ram[2]));
			jQuery('#ref-'+i).val(ram[0]);
			jQuery('#des-'+i).parent().attr({'data-content':ram[2]+' '+Drupal.t('unit'),'data-stock':ram[2]});
			jQuery('#qty-'+i).attr('data-stock',ram[2]);
			jQuery('#pxu-'+i).val(ram[3]);
			Structure_StockClearDesignationAutocompleteList(); }	}

	Structure_StockClearDesignationAutocompleteList=function(){jQuery('ul#sales-autocomplete-list').html('').removeClass('show');jQuery('#sales-autocomplete-list').insertBefore('#table-sales-rows');}

	Structure_ClearSelectToolCheckbox=function(){jQuery('#icheckbox').attr('data-state','none').removeClass('icon-checkmark7 icon-minus8');}

	Structure_Icheckbox=function(){
		jQuery('#icheckbox').removeClass('icon-checkmark7 icon-minus8');
		if(jQuery('#icheckbox').attr('data-state')=='none')jQuery('#icheckbox').removeClass('icon-checkmark7 icon-minus8');
		if(jQuery('#icheckbox').attr('data-state')=='full')jQuery('#icheckbox').removeClass('icon-minus8').addClass('icon-checkmark7');
		if(jQuery('#icheckbox').attr('data-state')=='part')jQuery('#icheckbox').removeClass('icon-checkmark7').addClass('icon-minus8'); };

	Structure_ListSelect=function(li){ switch (li) {
			case 'check-all'	: jQuery('i.icheck input').prop('checked',true); jQuery('#icheckbox').attr('data-state','full'); Structure_Icheckbox(); break;
			case 'check-none'	: jQuery('i.icheck input').prop('checked',false);jQuery('#icheckbox').attr('data-state','none'); Structure_Icheckbox(); break;
			default				: Structure_ListSelectOthers(li); Structure_Icheckbox(); } }

	Structure_ListSelectOthers=function(li){ var List={'check-saved':'iinfo-status','check-unsaved':'iinfo-unstatus','check-error':'iinfo-error'};
		jQuery('#table-sales-rows tbody td.select i.icheck input').prop('checked',false);
		jQuery('#table-sales-rows tbody td.select').each(function(){
			if(jQuery(this).find('.iinfo').hasClass(List[li]))jQuery(this).find('i.icheck input').prop('checked',true);
		});
		Structure_CheckboxState();Structure_Icheckbox(); }

	Structure_CheckboxState=function(){var checkex=jQuery('#table-sales-rows tbody tr').length, checked=jQuery('i.icheck input:checked').length;
		if(checkex==1){if(checked==1)jQuery('#icheckbox').attr('data-state','full');else jQuery('#icheckbox').attr('data-state','none');}
		if(checkex>1){
			if(checked==0)jQuery('#icheckbox').attr('data-state','none');
			if(checked==checkex)jQuery('#icheckbox').attr('data-state','full');
			if(checked<checkex&&checked>0)jQuery('#icheckbox').attr('data-state','part'); } }

	Structure_ValidateSaleFields=function(id,data){data=data||'';var valid=false,module='sales'; // VALIDATE AND NOTIFY ROW FIELDS
		var fields={sales:'.sale-row.sr-'+id+' input.main-form'};
		jQuery('.sr-'+id+' .ii.icheck.iinfo').removeClass('iinfo-error');
		jQuery(fields[module]).removeClass('error empty invalid negatif not-numeric unchosen overstock rupture');
		jQuery(fields[module]).each(function(){
			if(jQuery(this).attr('type')=='text'&&jQuery(this).val()!==''&&!jQuery('#ref-'+id).val()){jQuery(this).addClass('error unchosen');}
			if(jQuery('#des-'+id).val()!==''&&jQuery('#ref-'+id).val()&&!!data.length){
				jQuery.each(data.split('::'),function(k,v){if(v==jQuery('#des-'+id).val())valid=true;});
				if(!valid)jQuery('#des-'+id).addClass('error invalid'); }
			if(jQuery(this).attr('type')=='text'&&jQuery(this).val()=='')jQuery(this).addClass('error empty');
			if(jQuery(this).attr('type')=='number'&&!jQuery.isNumeric(jQuery(this).val()))jQuery(this).addClass('error not-numeric');
			if(jQuery(this).attr('type')=='number'&&jQuery(this).val()<0)jQuery(this).addClass('error negatif'); });
		if(jQuery('#qty-'+id).val()!==''&&!!parseInt(jQuery('.sr-'+id+' td.des').attr('data-stock'))&&parseInt(jQuery('#qty-'+id).attr('data-stock'))<0)
			jQuery('#qty-'+id).addClass('error overstock');
		if(jQuery('.sr-'+id+' td.des').attr('data-stock')=='0')jQuery('#des-'+id).addClass('error rupture');
		if(jQuery(fields[module]).hasClass('error')){ClearSalesNotification();SaleErroInfo(id);jQuery('.sr-'+id+' .ii.icheck.iinfo').addClass('iinfo-error');return false;}else{ClearSalesNotification(id);return true;} }

	Structure_ValidateSalesDateFormat=function(){ // DATE REGEX VALIDATOR
		var field='#date-sales-datepicker-popup-0',reg=/^(0?[1-9]|[12][0-9]|3[01])[\/](0?[1-9]|1[012])[\/]\d{4}$/;
		jQuery(field).removeClass('error error-date');
		if(!reg.test(jQuery(field).val())){jQuery(field).addClass('error error-date');DateErrorInfo();return false;}
		else{ClearDateNotification();return true;} }

	Structure_SerializeNgSalesDate=function(){
		var adn=jQuery('#table-sales-rows tbody tr').length,data='date='+jQuery('#date-sales input').val()+'&n_sales='+parseInt(adn-1,10),dels="&dels=0";
		for(var i=1,j=0;i<adn;i++,j++){
			data+='&mid'+j+'='+jQuery('#mid-'+i).val();
			data+='&ref'+j+'='+jQuery('#ref-'+i).val();
			data+='&qty'+j+'='+jQuery('#qty-'+i).val();
			data+='&pxu'+j+'='+jQuery('#pxu-'+i).val();
			data+='&rem'+j+'='+jQuery('#rem-'+i).val(); }
		if(!!jQuery('#cbd-sales-deleted').html()){var rows=jQuery('#cbd-sales-deleted').html().slice(0,-3).split(':::');
			dels="&dels="+rows.length;
			for(var i=0;i<rows.length;i++){
				var ram=rows[i].split('::');
				dels+='&delref'+i+'='+ram[0]+'&delqty'+i+'='+ram[1];
			}
		}data+=dels;
		return data;}

	Structure_SaveSalesDateProcess=function(){
		jQuery('#save-sales').addClass('disable');
		var inode=jQuery('#inode').val()?jQuery('#inode').val():'new',
				adn=jQuery('#table-sales-rows tbody tr').length,
				dateField='#date-sales-datepicker-popup-0',
				saleRows='#table-sales-rows .sale-row',
				rowFields='input.main-form',
				dateNotify=false;
		data=jQuery('#cbd-products-names').html();
		for(var i=1;i<adn;i++)Structure_ValidateSaleFields(i,data);
		Structure_ValidateSalesDateFormat();
		if(jQuery(rowFields).hasClass('error')||jQuery(saleRows).find('i').hasClass('red-info')||jQuery(dateField).hasClass('error')){NotifyNg('err','sales');return false;}
		else {Api_SaveSalesDate(inode);} }

	Structure_SalesRemoveSelectedLines=function(){var html='',row='';jQuery('#cbd-sales-deleted').html('');
		jQuery('i.icheck input').each(function(){
			if(jQuery(this).is(':checked')){
				var id=parseInt(jQuery(this).parent().attr('data-id')),
						qty=parseInt(jQuery('#qty-'+id).val()),
						adn=jQuery('#table-sales-rows tbody tr').length,
						ref=jQuery('#ref-'+id).val(),
						mid=jQuery('#mid-'+id).val();
				for(var i=(id+1);i<adn;i++){
					if(ref==jQuery('#ref-'+i).val()){
						var stock=parseInt(jQuery('#qty-'+i).attr('data-stock'))+qty,loadStock=parseInt(jQuery('#mid-'+i).attr('data-load-stock'))+qty;
						jQuery('#des-'+i).parent().attr({'data-content':stock+' '+Drupal.t('unit'),'data-stock':stock});
						jQuery('#qty-'+i).attr('data-stock',stock);
						jQuery('#mid-'+i).attr('data-load-stock',loadStock);	}	}
				if(jQuery('#inode').val()!=='new'){
					row=ref+'::'+qty+':::';
					html=jQuery('#cbd-sales-deleted').html();
					html+=row;
					jQuery('#cbd-sales-deleted').html(html);
				}
				jQuery('.sale-row.sr-'+id).remove();
			}
		})
		var adn=jQuery('#table-sales-rows tbody tr').length;
		jQuery('#table-sales-rows tbody tr').each(function(k,v){
			var id=jQuery(v).attr('data-id'),i=k+1,mid='input#mid-'+id,ref='input#ref-'+id,desv='input#desv-'+id,des='input#des-'+id,qty='input#qty-'+id,pxu='input#pxu-'+id,tot='input#tot-'+id,rem='textarea#rem-'+id,opcol='.op-row-'+id,icheck=opcol+' i.icheck',check=icheck+' input',imsg=opcol+' i.imsg',notif=imsg+' div';
			jQuery(v).removeClass('sr-'+id).addClass('sr-'+i).attr('data-id',i);
			jQuery(des+','+qty+','+pxu+','+rem).attr('data-id',i);
			jQuery(ref).attr('id','ref-'+i);
			jQuery(desv).attr('id','desv-'+i);
			jQuery(mid).attr({'id':'mid-'+i,'name':'mid-'+i});
			jQuery(des).attr({'id':'des-'+i,'name':'des-'+i});
			jQuery(qty).attr({'id':'qty-'+i,'name':'qty-'+i});
			jQuery(pxu).attr({'id':'pxu-'+i,'name':'pxu-'+i});
			jQuery(tot).attr({'id':'tot-'+i,'name':'tot-'+i});
			jQuery(rem).attr({'id':'rem-'+i,'name':'rem-'+i});
			jQuery(check).removeClass('icheck-'+id).addClass('icheck-'+i);
			jQuery(icheck).attr('data-id',i);
			jQuery(notif).removeClass(id).addClass(i);
			jQuery(imsg).attr('id','imsg-'+i);
			jQuery(opcol).removeClass('op-row-'+id).addClass('op-row-'+i);
			jQuery('#validate-sale-row').removeClass(id).addClass(i).attr('data-id',i);
		}) }

	Structure_SalesSwitchDateConfirmation=function(message){var btns = {};
		btns[Drupal.t('Yes')]=function(){jQuery('#date-sales input').val(jQuery('#date-sales input').data('old'));Structure_SaveSalesDateProcess();jQuery(this).remove();};
		btns[Drupal.t('No')]=function(){Structure_ClearSelectToolCheckbox();Api_LoadDateSales();jQuery(this).remove();};
		jQuery('<div></div>')
			.appendTo('body').html('<div><h6>'+message+'?</h6></div>')
			.dialog({
				modal: true, 
				title: Drupal.t('There are Sales Lines not saved'), 
				zIndex: 10000, 
				autoOpen: true,
				width: '350', 
				resizable: false,
				buttons: btns,
				close:function(event,ui){jQuery(this).remove();}
			})	}
	
	Structure_SavableRowConfirmation=function(adn){var btns={};
		btns[Drupal.t('Yes')]=function(){jQuery('#validate-sale-row').trigger('click');jQuery(this).remove();};
		btns[Drupal.t('No')]=function(){ClearSalesNotification(adn);jQuery('.sale-row.sr-'+adn+' input.main-form').removeClass('error empty invalid negatif not-numeric unchosen overstock rupture');Structure_SaveSalesDateProcess();jQuery(this).remove();};
		jQuery('<div></div>')
			.appendTo('body').html('<div><h6>'+Drupal.t('Do you want to save the last sales line ')+'?</h6></div>')
			.dialog({
				modal: true, 
				title: Drupal.t('Incomplete sales line data'), 
				zIndex: 10000, 
				autoOpen: true,
				width: '350', 
				resizable: false,
				buttons: btns,
				close:function(event,ui){jQuery(this).remove();}
			})
	}


})(jQuery);