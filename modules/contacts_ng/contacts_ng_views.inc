<?php

function views_add_contact_form_block($type){
	$contact=array(
		'customers'=>array(
			'id'=>138,
			'title'=>'Customer',),
		'suppliers'=>array(
			'id'=>139,
			'title'=>'Supplier',),);
	$form='<div id="add-'.$type.'" class="add-contact-block">
		<h2 class="block-title">'.t('New '.$contact[$type]['title']).'</h2><div class="add-contact-form"><span id="close-add-'.$type.'" class="close-contact icon-close2"></span>
			<div class="form-wrapper"><input type="text" class="contact-form" id="'.$type.'-name" placeholder="'.t('Name').'"></div>
			<div class="form-wrapper"><input type="text" class="contact-form" id="'.$type.'-company" placeholder="'.t('Company').'"></div>
			<div class="form-wrapper"><input type="text" class="contact-form" id="'.$type.'-telephone" placeholder="'.t('Telephone').'"></div>
	 		<input type="hidden" id="contact-type" value="'.$contact[$type]['id'].'">
		</div></div>';
	return$form;
}

function views_contacts_table_block($type){
	$contact=array('customers'=>'Customers','suppliers'=>'Suppliers');$single=array('customers'=>'Customer','suppliers'=>'Supplier');
	$output='
		<h2 class="block-title">'.t($contact[$type]).'</h2><div id="add-new-'.$type.'" class="title-master-op"><i class="icon-addressbook"></i><span>'.t('Add '.$single[$type]).'</span></div>
		'.views_add_contact_form_block($type).'
		<div id="block-'.$type.'">
			<div id="block-'.$type.'-filters" class="block-sidebar-filter">
				<div class="title-wrapper">
					<h4 class="block-title">'.t($contact[$type].' filter').'</h4>
				</div>
				<div class="filters-wrapper">
					<div class="'.$type.'-filter-wrapper">
						<input type="text" id="'.$type.'-filter" class="autocomplete-'.$type.' form-autocomplete names-filter" placeholder="'.t('Search in names ..').'"><ul id="'.$type.'-filter-autocomplete"></ul>
					</div>
				</div>
			</div>
			<div id="block-'.$type.'-content">'.views_contacts_table($type).'</div>
		</div>';
return $output;
}

function views_contacts_table($type){$contact=array('customers'=>138,'suppliers'=>139);
	$thead='<thead><tr><th></th><th>'.t('Name').'</th><th>'.t('Company').'</th><th>'.t('Telephone').'</th></tr></thead>';
	$tbody='<tbody>'.views_contacts_body($type).'</tbody>';
	$table='<table id="'.$type.'-table">'.$thead.$tbody.'</table>';
	return$table;
}

function views_contacts_body($type){$contact=array('customers'=>138,'suppliers'=>139);$rows='';$datas=contacts_ng_contacts_data($contact[$type],'*');
	foreach($datas as$data){$stack=$data;$data=explode('::',$data);
		$rows.='<tr class="'.$type.'-line-item '.$data[0].'" data-id="'.$data[0].'" data-data="'.$stack.'" data-type-ref="'.$contact[$type].'">
			<td class="operation fix-width-10">
				<i data-id="'.$data[0].'" data-type="request" class="icon-minus5 delete request" data-op="delete"></i>
				<i data-id="'.$data[0].'" data-type="response" class="icon-cancel3 hide cancel response" data-op="cancel"></i>
				<i data-id="'.$data[0].'" data-type="request" class="icon-pencil edit request" data-op="edit"></i>
				<i data-id="'.$data[0].'" data-type="response" class="icon-checkmark5 hide confirm response" data-op="confirm"></i></td>
			<td class="fix-width-30"><span data-id="'.$data[0].'" data-name="name">'.$data[1].'</span></td>
			<td class="fix-width-30"><span data-id="'.$data[0].'" data-name="company">'.$data[2].'</span></td>
			<td class="fix-width-30"><span data-id="'.$data[0].'" data-name="telephone" dir="ltr">'.$data[3].'</span></td>
		</tr>';
	}
	return$rows;
}
