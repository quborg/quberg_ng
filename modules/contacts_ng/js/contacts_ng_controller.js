(function($){

	Drupal.behaviors.Controller_ContactsNg_AddNewCustomer={ // ADD NEW ELEMENT
		attach:function(context,settings){
			jQuery('.add-contact-form input').live('keyup',function(){jQuery('#save-contacts').removeClass('disable');})
			jQuery('#save-contacts').live('click',function(){if(!jQuery(this).hasClass('disable')&&jQuery('#add-customers').css('display')=='block')Structure_ContactSave('new','customers');})
	}	};

	Drupal.behaviors.Controller_ContactsNg_Sidebar_Controller={ // SWITCH SIDEBAR MENU BLOCK
		attach:function(context,settings){
			var Menu={'customers-sidebar-contacts':'#customers-block',
								'suppliers-sidebar-contacts':'#suppliers-block',};
			jQuery('.contacts-menu-sidebar-link').click(function(){var Lnk=this.getAttribute("id");
				jQuery.each(Menu,function(k,v){
					if(k!==Lnk){jQuery(v).hide();jQuery('#'+k).removeClass('active-vmsl');ClearNotifications('sheets');}
					else{jQuery(v).show();jQuery('#'+k).addClass('active-vmsl');}
				});
			});
	}	};

	Drupal.behaviors.Controller_ContactsNg_ActionsTool={ // ACTIONS & BUTTONS TOOL
		attach:function(context,settings){
			jQuery('#add-new-customers').live('click',function(){jQuery('#block-customers').hide();jQuery('#add-customers').show();});
			jQuery('#close-add-customers').live('click',function(){jQuery('#block-customers').show();jQuery('#add-customers').hide();});
			jQuery('#add-new-suppliers').live('click',function(){jQuery('#block-suppliers').hide();jQuery('#add-suppliers').show();});
			jQuery('#close-add-suppliers').live('click',function(){jQuery('#block-suppliers').show();jQuery('#add-suppliers').hide();});
			jQuery('#refresh-contacts').live('click',function(){if(jQuery('#block-customers').css('display')=='block')Api_ContactsRefresh('customers');});

			jQuery('#block-contacts-ng-contacts-ng .operation i').live('click',function(){var id=jQuery(this).attr('data-id'),op=jQuery(this).attr('data-op'),el='.customers-line-item.'+id+' i';
				jQuery(el).toggleClass('hide');
				if(jQuery(this).attr('data-type')=='request')jQuery(el+'.response').attr('data-action',op);
				if(jQuery(this).attr('data-op')=='confirm'&&jQuery(this).attr('data-action')=='delete')Api_DeleteContact(id);
				if(jQuery(this).attr('data-op')=='edit')jQuery('.customers-line-item.'+id+' span').attr('contenteditable','');
				if(jQuery(this).attr('data-op')=='cancel'&&jQuery(this).attr('data-action')=='edit'){var ram=jQuery('.customers-line-item.'+id).attr('data-data').split('::');
					jQuery('.customers-line-item.'+id+' span').removeAttr('contenteditable').each(function(){
						if(jQuery(this).attr('data-name')=='name')jQuery(this).html(ram[1]);
						if(jQuery(this).attr('data-name')=='company')jQuery(this).html(ram[2]);
						if(jQuery(this).attr('data-name')=='telephone')jQuery(this).html(ram[3]);})}
				if(jQuery(this).attr('data-op')=='confirm'&&jQuery(this).attr('data-action')=='edit'){
					if(jQuery('#block-customers').css('display')=='block')Structure_ContactSave(id,'customers');};})
	}	};


})(jQuery);
