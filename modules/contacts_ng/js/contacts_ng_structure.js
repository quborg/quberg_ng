(function($){

	Structure_ContactSave=function(id,type){
		jQuery('#save-contacts').addClass('disable');
		id=='new'?Structure_ValidateNewContactForm(type):Structure_ValidateOldContactForm(id,type);
		name=id=='new'?'.add-contact-form input':'.customers-line-item.'+id+' span[data-name="name"]';
			if(jQuery(name).hasClass('error')){NotifyNg('err','contacts');return false;}
		Api_ContactSave(id,type);}

	Structure_ValidateNewContactForm=function(type){jQuery('#'+type+'-name').removeClass('error empty');
		if(jQuery('#'+type+'-name').val().replace(/\ /g,'')=='')jQuery('#'+type+'-name').addClass('error empty');}

	Structure_SerializeNewContact=function(id,type){var data='id='+id,contact={'customers':138,'suppliers':139};
		data+='&name='+jQuery('#'+type+'-name').val();
		data+='&company='+jQuery('#'+type+'-company').val();
		data+='&telephone='+jQuery('#'+type+'-telephone').val();
		data+='&type='+contact[type];
		return data;}

	Structure_ValidateOldContactForm=function(id,type){
		jQuery('.'+type+'-line-item.'+id+' span[data-name="name"]').removeClass('error empty');
		if(jQuery('.'+type+'-line-item.'+id+' span[data-name="name"]').text().replace(/\ /g,'')=='')jQuery('.'+type+'-line-item.'+id+' span[data-name="name"]').addClass('error empty');
		else jQuery('.'+type+'-line-item.'+id+' span').removeAttr('contenteditable');}

	Structure_SerializeOldContact=function(id,type){var data='id='+id,contact={'customers':138,'suppliers':139};
		data+='&name='+jQuery('.'+type+'-line-item.'+id+' span[data-name="name"]').text();
		data+='&company='+jQuery('.'+type+'-line-item.'+id+' span[data-name="company"]').text();
		data+='&telephone='+jQuery('.'+type+'-line-item.'+id+' span[data-name="telephone"]').text();
		data+='&type='+contact[type];
		console.log(data);
		return data;}


})(jQuery);
