(function($){

	Api_ContactSave=function(id,type){var data=id=='new'?Structure_SerializeNewContact(id,type):Structure_SerializeOldContact(id,type);
		jQuery.ajax({url:'contacts_ng/save/'+id,type:'POST',data:data,
				beforeSend:function(){jQuery('#wait-action-load').addClass('show');},
				success:function(data){console.log(data);if(parseInt(jQuery(data).filter('contactid').text())){NotifyNg('stu','contacts');}else{console.log('--');NotifyNg('wrn','contacts');}},
				complete:function(){jQuery('#wait-action-load').removeClass('show');},
				error:function(){console.log('--');NotifyNg('wrn','contacts')},
		});
	}

	Api_ContactsRefresh=function(type){
		jQuery.ajax({url:'contacts_ng/contacts_rows/'+type,
				beforeSend:function(){jQuery('#wait-action-load').addClass('show');},
				success:function(data){jQuery('#'+type+'-table tbody').html(data);},
				complete:function(){jQuery('#wait-action-load').removeClass('show');},
				error:function(){console.log('--');NotifyNg('wrn','contacts')},
		});
	}

	Api_DeleteContact=function(id){
		jQuery.ajax({url:'contacts_ng/contact_delete/'+id,
				beforeSend:function(){jQuery('#wait-action-load').addClass('show');},
				success:function(data){if(jQuery(data).filter('span').text()=='success'){jQuery('.customers-line-item.'+id).remove();NotifyNg('stu','contacts');}
					if(jQuery(data).filter('span').text()=='not found')NotifyNg('err','contacts');},
				complete:function(){jQuery('#wait-action-load').removeClass('show');},
				error:function(){console.log('--');NotifyNg('wrn','contacts')},
		});
	}

})(jQuery);
