(function($){

	Api_SalesAutoCompleteDesignation=function(i,value,orizon,key){
		jQuery.ajax({url:'sales_ng/autocomplete/'+value,
			beforeSend:function(){jQuery('#des-'+i).css('background-position',orizon+' -16px');},
			success:function(data){Structure_BuildSalesProductsEntityList(i,data,key);},
			complete:function(){jQuery('#des-'+i).css('background-position',orizon+' 4px');},
			error:function(){console.log('--');NotifyNg('wrn','sales')},
		});	}

	Api_LoadDateSales=function(){
		jQuery.ajax({url:"sales_ng/load_date_sales/"+jQuery('#date-sales input').val().replace(/\//g,'-'),
			beforeSend:function(){jQuery('#wait-action-load').addClass('show');},
			success:function(data){jQuery('#date-sales input').data('old',jQuery('#date-sales input').val());
				jQuery('#inode').val(data[0]['inode']);
				var Nsales=data[0]['nrows'];
				if(Nsales)Structure_GetSalesRows("sales_ng/rows/"+data[0]['nrows']+"/2",'html',Nsales,1,data);
				else Structure_GetSalesRows("sales_ng/no_sales_sale_row",'html',0,0,'');},
			complete:function(){jQuery('#wait-action-load').removeClass('show');},
			error:function(){console.log('--');NotifyNg('wrn','sales')}, }) }

	Api_LoadRevisionSales=function(action){
		var date1=jQuery('#salesrevision-date-filter').val(),
			date2=jQuery('#salesrevision-todate-filter').val(),
			d1=!!date1?date1.replace(/\//g,'-'):'*',
			d2=!!date2?date2.replace(/\//g,'-'):'null',
			type='null';
		if(action=='refresh'){d1='*';jQuery('#block-salesrevision-filters input').val('');}
		var url='sales_ng/sales_revision/'+d1+'/'+d2+'/'+type+'/url';
		jQuery.ajax({url:url,
			beforeSend:function(){jQuery('#wait-action-load').addClass('show');},
			success:function(data){if(!!data.length)Structure_BuildRevisionSales(data);
				else{jQuery('#salesrevision-table tbody').html('<td>'+Drupal.t('No sales registred ..')+'</td><td></td><td></td><td></td><td></td>');jQuery('#salesrevision-total').html('0')}},
			complete:function(){jQuery('#wait-action-load').removeClass('show');},
			error:function(){console.log('--');NotifyNg('wrn','sales')},})}

	Api_SaveSalesDate=function(inode){
		jQuery.ajax({
			type: "POST",	url:'sales_ng/serialized_save_sales/'+inode,
			data: Structure_SerializeNgSalesDate(),
			beforeSend:function(){jQuery('#wait-action-load').addClass('show');},
			success:function(data){
				if(parseInt(jQuery(data).filter('inode').text())){
					jQuery('#inode').val(jQuery(data).filter('inode').text());
					var adn=jQuery('#table-sales-rows tbody tr').length;
					for(var i=1;i<adn;i++){jQuery('.icheck-'+i).parent().removeClass('iinfo-unstatus').addClass('iinfo-status');} // *****
					NotifyNg('stu','sales');
					jQuery('.sale-row').each(function(){var id=jQuery(this).attr('data-id');
						jQuery(this).find('td.des').attr({'data-loaded-ref':jQuery('#ref-'+id),'data-loaded-qty':jQuery('#qty-'+id)});
					});
					Api_LoadData_ProductsNames();
				}else{console.log('--');NotifyNg('wrn','sales');} },
			complete:function(){jQuery('#wait-action-load').removeClass('show');},
			error:function(){console.log('--');NotifyNg('wrn','sales');}
		}) };


})(jQuery);
