<?php
function views_table_sales_rows(){
  $table=array('#prefix'=>'<div id="sales-table"><table id="table-sales-rows"><thead><tr><th class="select"><div class="select-tool" id="sales-select-tool"><div id="icheckbox" class="icheckbox fake-checkbox" data-state="none" title="Select"></div><div class="select-tool-btn" id="sales-select-tool-btn" title="Select"><i class="icon-caret-down"></i></div><div class="select-tool-content" id="sales-select-tool-content"><ul><li id="check-all">'.t('All').'</li><li id="check-none">'.t('None').'</li><li id="check-saved">'.t('Saved').'</li><li id="check-unsaved">'.t('Unsaved').'</li><li id="check-error">'.t('Error').'</li></ul></div></div></th><th class="add-sale"></th><th width="30%">'.t('Designation').'</th><th width="10%">'.t('Quantity').'</th><th width="10%">'.t('Unit Price').'</th><th width="10%">'.t('Total').'</th><th>'.t('Comment').'</th></tr></thead><tbody>','#suffix'=>'<tr><td></td><td></td><td>'.t(' Load sales ..').'</td><td></td><td></td><td></td><td></td></tr></tbody></table></div>');
  return$table;	  }
function views_sale_row($a,$flag){$tr='';$T=array();$T=$flag!=1?[1,$a+1]:[$a,$a+1];
  for($i=$T[0];$i<$T[1];$i++) // <input id="nano-id-'.$i.'" name="nano-id-'.$i.'" value="'.exec('date +%s%N').'" type="hidden"> :: Nano-time %s%N
    $tr.='<tr class="sale-row sr-'.$i.'" data-id="'.$i.'"><td class="select"><div class="op-column op-row-'.$i.'"><input id="mid-'.$i.'" name="mid-'.$i.'" type="hidden"><i class="ii icheck iinfo iinfo-unstatus" data-id="'.$i.'"><input type="checkbox" class="icheck-'.$i.'"></i><i id="imsg-'.$i.'" class="ii imsg icon-info"><div class="sales-notifications i-notifications '.$i.'"></div></i></div></td><td class="add-sale"></td><td width="30%" class="des col-name"><input type="hidden" id="ref-'.$i.'"><input type="hidden" id="desv-'.$i.'"><input type="text" id="des-'.$i.'" name="des-'.$i.'" class="main-form form-text form-autocomplete des-autocomplete autoc-actif" autocomplete="off" data-id="'.$i.'"></td><td class="col-qty" width="10%"><input class="main-form form-text quantity" type="number" id="qty-'.$i.'" name="qty-'.$i.'" data-id="'.$i.'" min="0"></td><td class="col-pxu" width="10%"><input class="main-form form-text" type="number" id="pxu-'.$i.'" name="pxu-'.$i.'" data-id="'.$i.'" min="0"></td><td class="col-tot" width="10%"><input type="number" id="tot-'.$i.'" name="tot-'.$i.'" disabled="disabled"></td><td><textarea class="main-form form-text" id="rem-'.$i.'" name="rem-'.$i.'" class="resizable form-textarea" rows="1" data-id="'.$i.'"></textarea></td></tr>';
  if($flag)return'<table id="sale-row">'.$tr.'</table>';
  else return array('#prefix'=>$tr);	}
function views_no_sales_sale_row(){
  return array('#prefix'=>'<table id="sale-row" init><tbody><tr class="sale-row sr-1" data-id="1"><td class="select"><div class="op-column op-row-1"><input id="mid-1" name="mid-1" value="'.round(microtime(true)*1000).'" type="hidden"><i class="ii icheck iinfo iinfo-unstatus" data-id="1"><div class="fake-checkbox"></div></i><i id="imsg-1" class="ii imsg icon-info"><div class="sales-notifications i-notifications 1"></div></i></div></td><td class="add-sale"><i id="validate-sale-row" class="icon-checkmark-circle 1" data-id="1"></i></td><td width="30%" class="des col-name"><input type="hidden" id="ref-1"><input type="hidden" id="desv-1"><input type="text" id="des-1" name="des-1" class="main-form form-text form-autocomplete des-autocomplete" autocomplete="off" data-id="1"></td><td class="col-qty" width="10%"><input class="main-form form-text quantity" type="number" id="qty-1" name="qty-1" data-id="1" min="0"></td><td class="col-pxu" width="10%"><input class="main-form form-text" type="number" id="pxu-1" name="pxu-1" data-id="1" min="0"></td><td class="col-tot" width="10%"><input type="number" id="tot-1" name="tot-1" disabled="disabled"></td><td><textarea class="main-form form-text" id="rem-1" name="rem-1" rows="1" data-id="1"></textarea></td></tr></tbody></table>');	}
function get_all_sales(){
  $rows='';
  $data=sales_sales_revision('*');
  foreach($data as$k=>$v)
    $rows.='<tr><td class="col-dte">'.$v['date'].'</td><td class="col-nom">'.$v['des'].'</td><td class="col-pur">'.$v['qty'].'</td><td class="col-pxu">'.$v['pxu'].'</td><td class="revision-total-item col-tot">'.$v["qty"]*$v["pxu"].'</td></tr>';
  return$rows;
}
function views_allsales_table(){
  $header='<thead><tr><th>'.t('Date').'</th><th>'.t('Designation').'</th><th>'.t('Quantity').'</th><th>'.t('Sale price').'</th><th>'.t('Total').'</th></tr></thead>';
  $rows='<tbody>'.get_all_sales().'</tbody>';
  return '<table id="salesrevision-table">'.$header.$rows.'</table>';
}
function views_pointofsale(){
  $build=array('#prefix'=>'<div id="block-pointofsale">',
    array('date'=>array('#type'=>'date_popup','#date_format'=>'d/m/Y','#default_value'=>date('Y-m-d'),'#id'=>'date-sales',),
              'nodeid'=>array('#prefix'=>'<input type="hidden" class="inode" id="inode">'),
              'core'=>views_table_sales_rows(),
              'formid'=>array('#prefix'=>'<input type="hidden" id="second-build-id" value="'.sale_node_build_and_token(1).'"><input type="hidden" id="second-form-token" value="'.sale_node_build_and_token(2).'">'),
    ),
    '#suffix'=>'</div>',
  );
  return$build;
}
function views_salesrevision(){
  return array('#prefix'=>'
    <div id="block-salesrevision">
      <h2 class="block-title">'.t('Sales revision').'</h2>
      <div id="block-salesrevision">
        <div id="block-salesrevision-filters" class="block-sidebar-filter">
          <div class="title-wrapper">
            <h4 class="block-title">'.t('Sales filter').'</h4>
            <span id="salesrevision-filter-action" class="salesrevision-filter-action button">'.t('search').'</span>
          </div>
          <div class="filters-wrapper">
            <div class="filter-wrapper">
              <input type="text" id="salesrevision-date-filter" class="date-filter" placeholder="'.t('Date').'">
            </div>
            <div class="filter-wrapper">
              <input type="text" id="salesrevision-todate-filter" class="todate-filter" placeholder="'.t('To date').'">
            </div>
          </div>
        </div>
        <div id="block-salesrevision-content">
          <div id="salesrevision-content-result" class="salesrevision-inline"><h4 class="block-title-revision">'.t('Total').'</h4><div id="salesrevision-total"></div><div id="revision-currency"></div></div>
          <div id="salesrevision-content-table" class="salesrevision-inline">'.views_allsales_table().'</div>
        </div>
      </div>
    </div>');
}
