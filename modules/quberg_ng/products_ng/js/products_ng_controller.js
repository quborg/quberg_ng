(function($){

	var module='stock';

	Drupal.behaviors.ProductsNg_Sidebar_Controller={ // SWITCH SIDEBAR MENU STOCK BLOCK
		attach:function(context,settings){
			var Menu={'products-sidebar-stock':'#block-products-ng-products-ng',
								'purchases-sidebar-stock':'#block-purchases-ng-all-purchases',};
			if(context==document)jQuery.each(Menu,function(k,v){jQuery(v).hide();});
			jQuery('.stock-menu-sidebar-link').click(function(){var Lnk=this.getAttribute("id");
				jQuery.each(Menu,function(k,v){
					if(k!==Lnk){jQuery(v).hide();jQuery('#'+k).removeClass('active-vmsl');ClearNotifications('stock');}
					else{jQuery(v).show();jQuery('#'+k).addClass('active-vmsl');}
				});
			});
	}	};

	Drupal.behaviors.ProductsNg_NewProductRowForm_Controller={ // ADD NEW FORM PRODUCT, VALIDATE AND ADD..
		attach:function(context,settings){
			jQuery('#add-product').click(function(){
				if(!jQuery('#new-product-row-form').length)Structure_NewProductRowForm();
				else Api_SaveRow('add');
			})
	}	};

	Drupal.behaviors.ProductsNG_TopBarTool_Controller={ // TOP BAR BOUTTON TOOL
		attach:function(context,settings){
			var WhenDisableSave=['#menu-toolbar-stock #save-stock','#products-sidebar-stock'];
			jQuery.each(WhenDisableSave,function(k,v){jQuery(v).live('click',function(){jQuery('#menu-toolbar-stock #save-stock').addClass('disable');})})
	}	};

	Drupal.behaviors.ProductsNG_ProductOperations_Controller={ // CRUD PRODUCTS
		attach:function(context,settings){
			jQuery('.pp-ops i').live('click',function(){var request='',response='',id=jQuery(this).parent().attr('data-row-id'),op=jQuery(this).attr('data-op');ClearNotifications('stock');
				if(op=='edit')Structure_EditRow(id);
				if(op=='delete')Api_DeleteRow(id,request);
				if(op=='confirm')Structure_ConfirmRequest(id,jQuery(this).attr('data-request'));
				if(op=='cancel')Structure_CancelRequest(id,jQuery(this).attr('data-request'));}) 
	}	};

	Drupal.behaviors.ProductsNG_ProductAutoCompleteDesignation_Controller={ // AUTOCOMPLETE PRODUCTS NAME
		attach:function(context,settings){
			jQuery('.op-edit-autocomplete').live('input',function(){
				var id=jQuery(this).parent().attr('data-row-id'),orizon=jQuery('body').hasClass('i18n-ar')?'2px':'100%',value=jQuery(this).val();
				if(jQuery(this).val()&&jQuery(this).val().replace(/\ /g, '')!=='')Api_AutoCompleteProductsNames(id,value,orizon);
				else Structure_ClearProductDesignationList();
			});
	}	};

	Drupal.behaviors.ProductsNG_ProductsCategoriesFilter_Controller={ // FILTER PRODUCT
		attach:function(context,settings){
			jQuery('#block-products-filter select option').live('click',function(){
				var filters='',select=jQuery('#block-products-filter select').val();
				jQuery('#block-products-filter select option:selected').each(function(){filters+=jQuery(this).val()+'::';});
				if(!select||select=='-')filters='all';
				else filters=filters.slice(0,-2);
				Api_LoadProductsCategoryFilter(filters);
			})
	}	};

	Drupal.behaviors.ProductsNG_CategoriesManager_Controller={ // CATEGORIES MANAGER
		attach:function(context,settings){var matchCats=[],backup='',i=0;
			jQuery('#manage-categories.actif').live('click',function(){jQuery(this).removeClass('actif');i=0;matchCats=[];backup=jQuery('#products-filter-category-select-wrapper').html();Structure_BuildCategoriesManager('open');});
			jQuery('#cat-man-cancel').live('click',function(){Structure_BuildCategoriesManager('close',backup);});
			jQuery('#cat-man-save').live('click',function(){Api_SaveCategoriesManager(matchCats);});
			jQuery('#cat-man-add').live('click',function(){Structure_AddNewCategoryToManager();});
			jQuery('#manage-terms i.icon-cancel3').live('click',function(){var id=jQuery(this).parent().attr('id');
				if(id!=='new')matchCats.push(id);
				jQuery(this).parent().hide('fast').remove();
			});
	}	};

	Drupal.behaviors.ProductsNG_ProductsRefreshProducts_Controller={ // REFRESH PRODUCTS
		attach:function(context,settings){
			jQuery('#refresh-products').live('click',function(){Api_LoadProductsCategoryFilter('all');});
	}	};

	Drupal.behaviors.ProductsNG_EditableProductName={ // Save Editable Product Name
		attach:function(context,settings){
			jQuery('#editable-product-title').live('keyup',function(){
				if(!jQuery('#save-product-title').length)jQuery(this).removeAttr('contenteditable').html('<span contenteditable>'+jQuery(this).text()+'</span>').prepend('<i id="save-product-title" class="icon-checkmark-circle"></i>');
				jQuery('#editable-product-title span').focus();
			});
			jQuery('#save-product-title').live('click',function(){var ProductsNameTitle=jQuery('#editable-product-title span').text();
				API_SaveUserVariables(ProductsNameTitle);
				jQuery('#save-product-title').remove();
			});
	}	};

})(jQuery);
