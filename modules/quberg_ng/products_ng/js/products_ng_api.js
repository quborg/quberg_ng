(function($){

	var module='stock';

	Api_AutoCompleteProductsNames=function(id,value,orizon){ // AUTOCOMPLETE PRODUCTS
		jQuery.ajax({url:'products_ng/autocomplete/'+value,
			beforeSend:function(){jQuery('#name-'+id).css('background-position',orizon+' -16px');},
			success:function(data){var timeCall=jQuery.now();Structure_BuildProductsNamesList(data,timeCall,id);},
			complete:function(){jQuery('#name-'+id).css('background-position',orizon+' 4px');},
			error:function(){NotifyNg('wrn','stock');},}); }

	Api_SaveRow=function(ctl){ // SAVE NEW PRODUCT LINE
		if(Structure_ValidateProductFields('new',module))
			jQuery.ajax({url:'products_ng/update_product',type:'POST',data:Structure_SerializeProductNg('new'),
				beforeSend:function(){jQuery('#wait-action-load').addClass('show');},
				success:function(data){if(parseInt(jQuery(data).filter('node_id').text())){var id=jQuery(data).filter('node_id').text();NotifyNg('stu','stock');Structure_NewRebuildGlassRow(id);Api_LoadData_ProductsNames();if(ctl=='add')Structure_NewProductRowForm();return true;}else{NotifyNg('wrn','stock');return false;}},
				complete:function(){jQuery('#wait-action-load').removeClass('show');},
				error:function(){NotifyNg('wrn','stock');return false;},});
		else{NotifyNg('err',module,'new');return false;} }

	Api_UpdateRow=function(id){ // UPDATE PRODUCT LINE
		if(Structure_ValidateProductFields(id,module))
			jQuery.ajax({url:'products_ng/update_product',type:'POST',data:Structure_SerializeProductNg(id),
				beforeSend:function(){jQuery('#wait-action-load').addClass('show');},
				success:function(data){if(parseInt(jQuery(data).filter('node_id').text())){NotifyNg('stu','stock');Structure_RebuildGlassRow(id);Structure_PPopsToggle(id,'response');Api_LoadData_ProductsNames();}else{console.log(data); NotifyNg('wrn','stock');}},
				complete:function(){jQuery('#wait-action-load').removeClass('show');},
				error:function(){NotifyNg('wrn','stock')},});}

	Api_DeleteRow=function(id,request){ // DELETE PRODUCT
		if(request=='delete'){
			jQuery.ajax({url:'products_ng/delete_product/'+id,
				beforeSend:function(){jQuery('#wait-action-load').addClass('show');},
				success:function(data){if(jQuery(data).filter('span').text()=='success'){NotifyNg('stu','stock');jQuery('tr.row-id-'+id).hide('fast').remove();Structure_PPopsToggle(id,'response');Api_LoadData_ProductsNames();}if(jQuery(data).filter('span').text()=='not found')NotifyNg('err','stock');},
				complete:function(){jQuery('#wait-action-load').removeClass('show');},
				error:function(){NotifyNg('wrn','stock')},});
		}else{jQuery('.pp-ops.'+id+' i').attr('data-request','delete');Structure_PPopsToggle(id,'request');}}

	Api_LoadProductsCategoryFilter=function(filters){ // PRODUCTS CATEGORY FILTER
		jQuery.ajax({url:'products_ng/products/'+filters,
			beforeSend:function(){jQuery('#wait-action-load').addClass('show');},
			success:function(data){jQuery('#table-products tbody').html(jQuery(data).find('tbody').html());},
			complete:function(){jQuery('#wait-action-load').removeClass('show');Structure_RestructSelect();},
			error:function(){NotifyNg('wrn','stock')},
		});	}

	Api_DeleteCategoriesProducts=function(ids){ // DELETE CATEGORIES
		jQuery.ajax({url:'products_ng/delete_categories',data:ids,type:'POST',//dataType:'json',
			beforeSend:function(){jQuery('#wait-action-load').addClass('show');},
			success:function(data){NotifyNg('stu','stock');},
			complete:function(){jQuery('#wait-action-load').removeClass('show');},
			error:function(){NotifyNg('wrn','stock')}, // error:function(jqXHR,textStatus,errorThrown){console.log(errorThrown.message);}
		});	}

	Api_SaveCategoriesManager=function(ids){ // SAVE MANAGER CATEGORIES
		jQuery.ajax({url:'products_ng/manager_save_categories',data:Structure_SerializeCategoriesNg(),type:'POST',
			beforeSend:function(){jQuery('#wait-action-load').addClass('show');},
			success:function(data){jQuery('#products-filter-category-select-wrapper').html(data);Structure_ResizeBlockFilterCategory();jQuery('#manage-categories').addClass('actif');},
			complete:function(){jQuery('#wait-action-load').removeClass('show');Api_LoadProductsCategoryFilter('all');},
			error:function(){NotifyNg('wrn','stock')},
		});
		if(ids.length){var data='';jQuery.each(ids,function(k,v){data+=k+'='+v+'&';});Api_DeleteCategoriesProducts(data);} }

	Api_LoadData_ProductsNames=function(){
		jQuery.ajax({url:'products_ng/products_names',
			success:function(data){if(data!=='wrn')jQuery('#content-block-data #cbd-products-names').html(data);},
			error:function(){console.log('--');},}); }

	API_SaveUserVariables=function(ProductsNameTitle){
		console.log(ProductsNameTitle);
		jQuery.ajax({url:'products_ng/user_variables',data:'productsnametitle='+ProductsNameTitle,type:'POST',
		success:function(data){console.log(data);} 
	}); }


})(jQuery);
