(function($){

	timeLog=0;

	Drupal.behaviors.ProductsNG_RestructSelect_Structure={attach:function(context,settings){
		Structure_RestructSelect();
		if(jQuery('#products-filter-category-select').length)Structure_ResizeBlockFilterCategory();
		if(jQuery('#editable-product-title').length)jQuery('#editable-product-title').html(Drupal.settings.User.ProductsNameTitle);
	}};

	Structure_NewProductRowForm=function(){jQuery('#table-products tbody').prepend(Drupal.settings.Products_ng.NewProductRowForm);}

	Structure_ProductsBlurAutoCompleteList=function(id){jQuery(document).live('click',function(e){if(jQuery(e.target)[0]!==jQuery('.stock-autocomplete-list.'+id)[0]&&jQuery(e.target)[0]!==jQuery('#name-'+id)[0])Structure_ClearProductDesignationList();})};

	Structure_ResizeBlockFilterCategory=function(){document.getElementById('products-filter-category-select').style.resize='vertical';}

	Structure_RestructSelect=function(){jQuery('#table-products span[class*="select-list-"]').each(function(){var option=jQuery(this).attr('data-original-id');jQuery(this).find('select').attr('disabled','disabled').find('option').each(function(){if(jQuery(this).val()==option)jQuery(this).attr('selected','selected')});});}

	Structure_EditRow=function(id){
		jQuery('tr.row-id-'+id+' .pp-holder').each(function(){var type=jQuery(this).attr('data-type');
			if(type=='name'){jQuery(this).attr('data-original-value',jQuery(this).find('span').text())
				.html('<input id="name-'+id+'" class="input-fix-width-30 op-edit-autocomplete form-autocomplete" type="text" data-type="'+type+'" value="'+jQuery(this).attr('data-original-value')+'"><ul class="autocomplete-list stock-autocomplete-list '+id+'"></ul>');}
			if(type=='pur'){jQuery(this).attr('data-original-value',jQuery(this).find('span').text())
				.html('<input class="input-fix-width-15" type="number" data-type="'+type+'" value="'+jQuery(this).attr('data-original-value')+'">');}
			if(type=='pxu'){jQuery(this).attr('data-original-value',jQuery(this).find('span').text())
				.html('<input class="input-fix-width-15" type="number" data-type="'+type+'" value="'+jQuery(this).attr('data-original-value')+'">');}
			if(type=='qty'){jQuery(this).attr('data-original-value',jQuery(this).find('span').text())
				.html('<input class="input-fix-width-15" type="number" data-type="'+type+'" value="'+jQuery(this).attr('data-original-value')+'">');}
			if(type=='cat'){jQuery(this).attr('data-original-value',jQuery(this).find('select').val());
				jQuery(this).find('select').attr('disabled',false);}
		});jQuery('.pp-ops.'+id+' i').attr('data-request','edit');Structure_PPopsToggle(id,'request');}

	Structure_ConfirmRequest=function(id,request){if(request=='save')Api_SaveRow();if(request=='edit')Api_UpdateRow(id);if(request=='delete')Api_DeleteRow(id,'delete');}

	Structure_CancelRequest=function(id,request){if(request=='delete')Structure_PPopsToggle(id,'response');if(request=='save')jQuery('#table-products tr#new-product-row-form').hide('fast').remove();if(request=='edit'){Structure_RollBackEdit(id);Structure_PPopsToggle(id,'response');};ClearNotifications('stock',id);}

	Structure_RollBackEdit=function(id){
		jQuery('tr.row-id-'+id+' .pp-holder').each(function(){var type=jQuery(this).attr('data-type');
			if(type=='name')jQuery(this).html('<span>'+jQuery(this).attr('data-original-value')+'</span>');
			if(type=='pur') jQuery(this).html('<span>'+jQuery(this).attr('data-original-value')+'</span>');
			if(type=='pxu') jQuery(this).html('<span>'+jQuery(this).attr('data-original-value')+'</span>');
			if(type=='qty') jQuery(this).html('<span>'+jQuery(this).attr('data-original-value')+'</span>');
			if(type=='cat'){option=jQuery(this).attr('data-original-id');jQuery(this).find('select').attr('disabled','disabled').find('option').each(function(){if(jQuery(this).val()==option)jQuery(this).attr('selected','selected')});}
		});ClearNotifications('stock');}

	Structure_RebuildGlassRow=function(id){
		jQuery('tr.row-id-'+id+' .pp-holder').each(function(){var type=jQuery(this).attr('data-type');
			if(type=='name')jQuery(this).html('<span>'+jQuery(this).find('input').val()+'</span>');
			if(type=='pur') jQuery(this).html('<span>'+jQuery(this).find('input').val()+'</span>');
			if(type=='pxu') jQuery(this).html('<span>'+jQuery(this).find('input').val()+'</span>');
			if(type=='qty') jQuery(this).html('<span>'+jQuery(this).find('input').val()+'</span>');
			if(type=='cat') jQuery(this).find('select').attr('disabled','disabled');
		});}

	Structure_NewRebuildGlassRow=function(id){
		jQuery('#new-product-row-form').removeClass('row-id-new').addClass('row-id-'+id).attr('data-product-id',id).find('.pp-ops.new').removeClass('new').addClass(id).attr('data-row-id',id).parent().parent().find('.pp-holder').each(function(){var type=jQuery(this).attr('data-type');
			if(type=='name'){jQuery(this).html('<span>'+jQuery(this).find('input').val()+'</span>').attr('data-row-id',id);}
			if(type=='pur') {jQuery(this).html('<span>'+jQuery(this).find('input').val()+'</span>').attr('data-row-id',id);}
			if(type=='pxu') {jQuery(this).html('<span>'+jQuery(this).find('input').val()+'</span>').attr('data-row-id',id);}
			if(type=='qty') {jQuery(this).html('<span>'+jQuery(this).find('input').val()+'</span>').attr('data-row-id',id);}
			if(type=='cat') {jQuery(this).removeClass('select-list-new').find('select').attr('disabled','disabled');}
		});jQuery('#new-product-row-form').removeAttr('id');Structure_PPopsToggle(id,'response');}

	Structure_SerializeProductNg=function(id){var data="id="+id,id_data_str='.row-id-'+id+' .pp-holder[data-type=';
		data+='&name='+jQuery(id_data_str+'"name"] input').val();
		data+='&pur='+jQuery(id_data_str+'"pur"] input').val();
		data+='&pxu='+jQuery(id_data_str+'"pxu"] input').val();
		data+='&qty='+jQuery(id_data_str+'"qty"] input').val();
		data+='&cat='+jQuery(id_data_str+'"cat"] select').val();
		return data;}

	Structure_ValidateProductFields=function(id,module){
		var fields={stock:'.product-row.row-id-'+id+' .pp-holder input,.select-list-'+id+' select'};
		jQuery(fields[module]).removeClass('error empty negatif not-numeric select-error');
		jQuery(fields[module]).each(function(){
			if(jQuery(this).attr('type')=='text'&&jQuery(this).val()=='')jQuery(this).addClass('error empty');
			if(jQuery(this).attr('type')=='number'&&jQuery(this).val()=='')jQuery(this).addClass('error not-numeric');
			if(jQuery(this).attr('type')=='number'&&jQuery(this).val()<0)jQuery(this).addClass('error negatif');
			if(jQuery(this).find('option:selected').val()=='-')jQuery('.select-list-'+id+' select').addClass('error select-error');
		});
		if(jQuery(fields[module]).hasClass('error')){NotifyNg('err',module,id);return false;}else{ClearNotifications(module,id);return true;}}

	Structure_PPopsToggle=function(id,op){if(op=='request'){jQuery('.pp-ops.'+id+' i.request').addClass('hide');jQuery('.pp-ops.'+id+' i.response').removeClass('hide');}if(op=='response'){jQuery('.pp-ops.'+id+' i.response').addClass('hide');jQuery('.pp-ops.'+id+' i.request').removeClass('hide');}}

	Structure_ClearProductDesignationList=function(){jQuery('ul.stock-autocomplete-list').html('').removeClass('show');}

	Structure_AddNewCategoryToManager=function(){var newline='<div id="new"><input type="text" data-id="new"><i class="icon-cancel3"></i></div>';jQuery(newline).prependTo(jQuery('#manage-terms'));}

	Structure_BuildProductsNamesList=function(data,timeCall,id){
		if(timeCall>timeLog){
			timeLog=timeCall;
			Structure_ClearProductDesignationList();
			var li='',input='.product-row.row-id-'+id+' input.op-edit-autocomplete';
			for(var j=0;j<data.length;j++){li+='<li>'+data[j]+'</li>';};
			if(li!=='')jQuery('ul.stock-autocomplete-list.'+id).append(li).addClass('show').css('width',jQuery(input).width()-8);
			Structure_ProductsBlurAutoCompleteList(id);
			jQuery('ul.stock-autocomplete-list.'+id+' li').click(function(){jQuery(input).val(jQuery(this).text());Structure_ClearProductDesignationList()});	}	}

	Structure_SerializeCategoriesNg=function(){var data="";jQuery('#manage-terms div').each(function(k){data+='&'+k+'='+jQuery(this).attr('id')+'::'+jQuery(this).find('input').val();});return data;}

	Structure_BuildCategoriesManager=function(state,bkp){
		if(state=='open'){
			var dyn_cat='<div id="manage-terms">',close='<i class="icon-cancel3"></i>';
			jQuery('select#products-filter-category-select option').each(function(){var text=jQuery(this).text(),tid=jQuery(this).val();
				if(tid!=='-')dyn_cat+='<div id="'+tid+'"><input type="text" data-id="'+tid+'" value="'+text+'">'+close+'</div>';});
			jQuery('#products-filter-category-select-wrapper').html(dyn_cat+'</div><span id="cat-man-add" class="cat-man">'+Drupal.t('Add')+'</span><span id="cat-man-save" class="cat-man">'+Drupal.t('Save')+'</span><span id="cat-man-cancel" class="cat-man">'+Drupal.t('Cancel')+'</span>'); }
		if(state=='close'){jQuery('#products-filter-category-select-wrapper').html(bkp);Structure_ResizeBlockFilterCategory();jQuery('#manage-categories').addClass('actif');}	}


})(jQuery);
