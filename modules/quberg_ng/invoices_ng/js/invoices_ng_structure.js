(function($){

	type={128:'Order',127:'Quote'};
	status={140:'Draft',134:'Open',136:'Paid'};

	Structure_InvoiceSaveProcess=function(){
		jQuery('#save-sheets').addClass('disable');
		var id=jQuery('#invoice-id').val();
		Structure_validateInvoice();
		if(jQuery('#invoice-content .tocheck').hasClass('error')){NotifyNg('err','sheets');return false;}
		else Api_InvoiceSave(id);
	}

	Structure_SerializeInvoice=function(){var data='',j=0,dels='&dels=0',
		id=jQuery('#invoice-id').val(),
		stk_id=jQuery('#invoice-id').attr('data-stk-id'),
		customer=jQuery('#customer-name').attr('data-ref')+'::'+jQuery('#customer-name').val(),
		date=jQuery('#invoice-date').val(),
		type=jQuery('#invoice-type-select').val(),
		status=jQuery('#invoice-status-select').val(),
		subtotal=jQuery('#subtotal').html(),
		downpayment=jQuery('#downpayment').val(),
		amount=jQuery('#amount').html(),
		alphabetic=jQuery('#alphabetic-amount').html();
		comment=JSON.stringify(jQuery('textarea#comment').val()).replace(/"/g,'');
		data='id='+id+'&stk_id='+stk_id+'&sales_type=invoice'+'&customer='+customer+'&type='+type+'&status='+status+'&date='+date+'&subtotal='+subtotal+'&downpayment='+downpayment+'&amount='+amount+'&alphabetic='+alphabetic+'&comment='+comment;
		jQuery('#invoice-table tbody tr').each(function(){var id=jQuery(this).attr('data-id');
			if(!!jQuery('#inv-des-'+id).attr('data-ref')){
				data+='&mid'+j+'='+jQuery('#inv-mid-'+id).val();
				data+='&ref'+j+'='+jQuery('#inv-des-'+id).attr('data-ref');
				data+='&des'+j+'='+jQuery('#inv-des-'+id).attr('data-ref')+'::'+jQuery('#inv-des-'+id).val();
				data+='&qty'+j+'='+jQuery('#inv-qty-'+id).val();
				data+='&pxu'+j+'='+jQuery('#inv-pxu-'+id).val();
				data+='&rem'+j+'=';
				j++; }
		});data+='&n_sales='+j;
		if(!!jQuery('#cbd-inv-stk-deleted').html()){var rows=jQuery('#cbd-inv-stk-deleted').html().slice(0,-3).split(':::');
			dels="&dels="+rows.length;
			for(var i=0;i<rows.length;i++){
				var ram=rows[i].split('::');
				dels+='&delref'+i+'='+ram[0]+'&delqty'+i+'='+ram[1]; }
		}data+=dels;
		return data;
	}

	Structure_InvoiceWatcheSales=function(data){console.log('watch');
		var flag=jQuery('#data-flag'),
				data=JSON.parse('{"'+data.replace(/&/g,'","').replace(/=/g,'":"')+'"}');
		if(data.type=='128'&&data.status!=='140')Api_InvoiceStoreSync(data.stk_id);
		if(flag.attr('data-type')=='128'&&flag.attr('data-status')!=='140'&&(data.type=='127'||data.status=='140')){Api_InvoiceStoreDeSync(data.stk_id);Api_UpdateInvoiceStkField('none',data.id);}
	}

	Structure_validateInvoice=function(){
		var date='#invoice-date',data_prods=jQuery('#cbd-products-names').html(),data_custos=jQuery('#cbd-customers-names').html(),reg=/^(0?[1-9]|[12][0-9]|3[01])[\/](0?[1-9]|1[012])[\/]\d{4}$/;
		jQuery('.tocheck').removeClass('error empty unchosen invalid negatif not-numeric error-date');
		if(!reg.test(jQuery(date).val()))jQuery(date).addClass('error error-date');
		if(jQuery('#customer-name').val()=='')jQuery('#customer-name').addClass('error empty');
		if(!jQuery('#customer-name').attr('data-ref')){jQuery('#customer-name').addClass('error unchosen');}
		if(jQuery('#customer-name').val()!==''&&!!jQuery('#customer-name').attr('data-ref')&&!!data_custos.length){var valid=false;
			jQuery.each(data_custos.split('::'),function(k,v){if(v==jQuery('#customer-name').val()){valid=true;return false;}});
			if(!valid)jQuery('#customer-name').addClass('error invalid');}
		jQuery('#invoice-table tbody tr').each(function(){var id=jQuery(this).attr('data-id');
			if(!jQuery('#inv-des-'+id).attr('data-ref')&&jQuery('#inv-des-'+id).val().replace(/\ /g,'')!==''){jQuery('#inv-des-'+id).addClass('error unchosen');}
			if(jQuery('#inv-des-'+id).val()!==''&&!!jQuery('#inv-des-'+id).attr('data-ref')&&!!data_prods.length){var valid=false;
				jQuery.each(data_prods.split('::'),function(k,v){if(v==jQuery('#inv-des-'+id).val()){valid=true;return false;}});
				if(!valid)jQuery('#inv-des-'+id).addClass('error invalid');}
			if(!jQuery.isNumeric(jQuery('#inv-qty-'+id).val()))jQuery('#inv-qty-'+id).addClass('error not-numeric');
			if(!jQuery.isNumeric(jQuery('#inv-pxu-'+id).val()))jQuery('#inv-pxu-'+id).addClass('error not-numeric');
			if(jQuery('#inv-qty-'+id).val()<0)jQuery('#inv-qty-'+id).addClass('error negatif');
			if(jQuery('#inv-pxu-'+id).val()<0)jQuery('#inv-pxu-'+id).addClass('error negatif');})
		if(!jQuery.isNumeric(jQuery('#downpayment').val()))jQuery('#downpayment').addClass('error not-numeric');
	}

	Structure_ChargeInvoiceSheet=function(data){//console.log(data);
		jQuery('#add-new-invoice').click();
		jQuery('#invoice-id').attr({'value':data.id,'data-stk-id':data.stk_id});
		jQuery('#data-flag').attr({'data-type':data.type.split('::')[0],'data-status':data.status.split('::')[0]});
		jQuery('#customer-name').attr({'data-ref':data.customer.split('::')[0],'value':data.customer.split('::')[1]});
		jQuery('#invoice-date').val(data.date);
		jQuery('#invoice-status-select').val(data.status.split('::')[0]);
		jQuery('#invoice-type-select').val(data.type.split('::')[0]).trigger('change');
		for(var i=0,j=1;i<data.n_items;i++,j++){
			jQuery('#inv-mid-'+j).val(data.mid[i]);
			jQuery('#inv-des-'+j).attr({'data-ref':data.des[i].split('::')[0],'value':data.des[i].split('::')[1]});
			jQuery('#inv-qty-'+j).val(data.qty[i]);
			jQuery('#inv-pxu-'+j).val(data.pxu[i]);
			jQuery('#invoice-add-item').click();
		};
		jQuery('#subtotal').html(data.subtotal);
		jQuery('#downpayment').val(data.downpayment);
		jQuery('#amount').html(data.amount);
		jQuery('#alphabetic-amount').html(data.alphabetic);
		jQuery('textarea#comment').val(data.comment);
		jQuery('#invoice-table').click();
	}

	Structure_BuildInvoiceProductsList=function(i,data,key){var li='';
		for(var j=0;j<data.length;j++){var ram=data[j].split('::');li+='<li data-data="'+data[j]+'">'+ram[1]+'</li>';}
		if(li!==''&&jQuery('.invoice-des.'+i).hasClass('autoc-actif')&&key==window.InvoiceAutoComplete){
			Structure_ClearInvoiceAutocompleteList(i);
			jQuery('.invoice-autocomplete.'+i).append(li).addClass('show').css('width',jQuery('.invoice-des').width()-10);}
		jQuery('ul.invoice-autocomplete li').click(function(){var ram=jQuery(this).attr('data-data').split('::');
			jQuery('#inv-des-'+i).removeClass('autoc-actif').val(ram[1]).attr('data-ref',ram[0]).parent().attr('data-content',ram[3]+' '+Drupal.t('unit'));
			jQuery('#inv-pxu-'+i).val(ram[2]);
			Structure_ClearInvoiceAutocompleteList(i);
			jQuery('#invoice-table').click();	})}

	Structure_BuildInvoiceCustomerList=function(data,key,el,ul){var li='';
		for(var j=0;j<data.length;j++){var ram=data[j].split('::');li+='<li data-data="'+data[j]+'">'+ram[1]+'</li>';}
		if(li!==''&&jQuery(el).hasClass('autoc-actif')&&key==window.CustomerAutoComplete){
			Structure_ClearCustomerAutocompleteList(ul);
			jQuery(ul).append(li).addClass('show').css('width',jQuery(el).width()-8);}
		jQuery(ul+' li').click(function(){var ram=jQuery(this).attr('data-data').split('::');
			jQuery(el).removeClass('autoc-actif').val(ram[1]).attr('data-ref',ram[0]);
			jQuery('span.customer-name').html(ram[1]);
			Structure_ClearCustomerAutocompleteList(ul); })}

	Structure_UpdateFlag=function(){jQuery('#data-flag').attr({'data-type':jQuery('#invoice-type-select').val(),'data-status':jQuery('#invoice-status-select').val()});}
	
	Structure_ClearInvoiceAutocompleteList=function(id){jQuery('.invoice-autocomplete.'+id).html('').removeClass('show');}

	Structure_ClearCustomerAutocompleteList=function(ul){jQuery(ul).html('').removeClass('show');}

	Structure_ClearInvoiceSheet=function(){
		jQuery('#invoice-id').attr({'value':'new','data-stk-id':'none'});
		jQuery('#data-flag').attr({'data-type':'','data-status':''});
		jQuery('#invoice-status-select').val(140);
		jQuery('#invoice-type-select').val(127).trigger('change');
		jQuery('#customer-name').attr({'value':'','data-ref':''});
		jQuery('#customer-name-print').html('');
		jQuery('#invoice-table tbody').html('<tr id="invoice-product-item-1" class="invoice-product-item 1" data-id="1"><td class="operation inv-sale-item"><i id="invoice-add-item" class="icon-reply 1"></i></td><td class="inv-des" width="30%"><input id="inv-mid-1" value="'+jQuery.now()+'" type="hidden"><input type="text" id="inv-des-1" class="tocheck invoice-des form-autocomplete 1 autoc-actif flip-flop" autocomplete="off" data-id="1"><ul class="invoice-autocomplete 1 autocomplete-list"></ul></td><td width="10%"><input class="tocheck flip-flop" id="inv-qty-1" value="0" data-type="number"></td><td width="10%"><input class="tocheck flip-flop" id="inv-pxu-1" value="0.00" data-type="number"></td><td width="10%"><input type="number" id="inv-tot-1" class="tot" value="0" disabled="disabled"></td></tr>');
		jQuery('#subtotal').html(0.00);
		jQuery('#downpayment').val(0.00);
		jQuery('#downpayment-print').html(0.00);
		jQuery('#amount').html(0.00);
		jQuery('#vat').html(0.00);
		jQuery('#alphabetic-amount').html('-');
		jQuery('textarea#comment').val('');
		jQuery('#cbd-inv-stk-deleted').html('');
	}

})(jQuery);
