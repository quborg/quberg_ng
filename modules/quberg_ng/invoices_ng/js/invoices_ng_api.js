(function($){

	Api_InvoiceAutoComplete=function(i,value,orizon,key){
		jQuery.ajax({url:'invoices_ng/autocomplete/'+value,
			beforeSend:function(){jQuery('.invoice-des.'+i).css('background-position',orizon+' -16px');},
			success:function(data){Structure_BuildInvoiceProductsList(i,data,key);},
			complete:function(){jQuery('.invoice-des.'+i).css('background-position',orizon+' 4px');},
			error:function(){console.log('--');NotifyNg('wrn','sheets')},
	});	}

	Api_CustomerAutoComplete=function(value,orizon,key,el,ul){
		jQuery.ajax({url:'invoices_ng/customers/'+value,
			beforeSend:function(){jQuery(el).css('background-position',orizon+' -16px');},
			success:function(data){Structure_BuildInvoiceCustomerList(data,key,el,ul);},
			complete:function(){jQuery(el).css('background-position',orizon+' 4px');},
			error:function(){console.log('--');NotifyNg('wrn','sheets')},
	});	}

	Api_InvoiceSave=function(id){var xDATA=Structure_SerializeInvoice();
		jQuery.ajax({url:'invoices_ng/save_invoice/'+id,type:'POST',data:xDATA,
			beforeSend:function(){jQuery('#wait-action-load').addClass('show');},
			success:function(data){console.log('invid:'+jQuery(data).filter('invoiceid').text());
				if(parseInt(jQuery(data).filter('invoiceid').text())){
					jQuery('#invoice-id').val(jQuery(data).filter('invoiceid').text());NotifyNg('stu','sheets');jQuery('#refresh-sheets').click();
					Structure_InvoiceWatcheSales(xDATA);
				}else{console.log('--');NotifyNg('wrn','sheets');}
			},
			complete:function(){jQuery('#wait-action-load').removeClass('show');},
			error:function(){console.log('--');NotifyNg('wrn','sheets')},
	});	}

	Api_InvoiceStoreSync=function(stk_id){console.log('sync '+stk_id); // cbd-inv-sales-deleted
		var xDATA=Structure_SerializeInvoice();
		jQuery.ajax({url:'sales_ng/serialized_save_sales/'+stk_id,data:xDATA,type:'POST',
			beforeSend:function(){jQuery('#wait-action-load').addClass('show2');},
			success:function(data){
				if(parseInt(jQuery(data).filter('inode').text())){
					var stkid=jQuery(data).filter('inode').text(),invid=jQuery('#invoice-id').val();
					console.log('stkid:'+stkid);
					Structure_UpdateFlag();
					Api_UpdateInvoiceStkField(stkid,invid);
				}else{console.log('--');NotifyNg('wrn','sheets');} },
			complete:function(){jQuery('#wait-action-load').removeClass('show2');},
			error:function(){console.log('--');NotifyNg('wrn','sheets')},
		});
	}

	Api_InvoiceStoreDeSync=function(id){console.log('desync :'+id);
		if(id!=='none')
			jQuery.ajax({url:'sales_ng/invoice_date_sales_delete/'+id,
				success:function(data){if(jQuery(data).filter('span').text()=='success')jQuery('#refresh-sheets').click();else console.log(jQuery(data).filter('span').text())},
				error:function(){console.log('--');NotifyNg('wrn','sheets')},});
	}

	Api_UpdateInvoiceStkField=function(stkid,invid){jQuery('#invoice-id').attr('data-stk-id',stkid);
		jQuery.ajax({url:'invoices_ng/insert_invoice_stkid/'+invid+'/'+stkid,
			success:function(data){if(parseInt(jQuery(data).filter('invid').text())){console.log('update stock field = '+stkid);jQuery('#refresh-sheets').click();}},
			error:function(){console.log('--');NotifyNg('wrn','sheets')},
		});
	}

	Api_InvoiceDelete=function(id){
		jQuery.ajax({url:'invoices_ng/invoice_delete/'+id,
			beforeSend:function(){jQuery('#wait-action-load').addClass('show');},
			success:function(data){
				if(jQuery(data).filter('span').text()=='success'){
					var stkid=jQuery('#invoices-table .invoice-line-item.'+id).attr('data-data').split('::')[10];
					console.log('inv del : '+stkid);
					Api_InvoiceStoreDeSync(stkid);
					jQuery('.invoice-line-item.'+id).remove();
					NotifyNg('stu','sheets');}
				if(jQuery(data).filter('span').text()=='not found')NotifyNg('err','sheets');},
			complete:function(){jQuery('#wait-action-load').removeClass('show');},
			error:function(){console.log('--');NotifyNg('wrn','sheets')},
	});	}

	Api_InvoiceLoadData=function(id){
		jQuery.ajax({url:'invoices_ng/invoice_data_by_id/'+id,
			beforeSend:function(){jQuery('#wait-action-load').addClass('show');},
			success:function(data){Structure_ChargeInvoiceSheet(data);},
			complete:function(){jQuery('#wait-action-load').removeClass('show');},
			error:function(){console.log('--');NotifyNg('wrn','sheets')},
	});	}

	Api_SheetsRefresh=function(){
		jQuery.ajax({url:'invoices_ng/invoices_refresh',
			beforeSend:function(){jQuery('#wait-action-load').addClass('show');},
			success:function(data){jQuery('#invoices-table tbody').html(data);},
			complete:function(){jQuery('#wait-action-load').removeClass('show');},
			error:function(){console.log('--');NotifyNg('wrn','sheets')},
	});	}

	Api_SheetsFilters=function(n,d,t,s){var d=d.replace(/\//g,'-');
		jQuery.ajax({url:'invoices_ng/invoices_filters/'+n+'/'+d+'/'+t+'/'+s,
			beforeSend:function(){jQuery('#wait-action-load').addClass('show');},
			success:function(data){jQuery('#invoices-table tbody').html(data);},
			complete:function(){jQuery('#wait-action-load').removeClass('show');},
			error:function(){console.log('--');NotifyNg('wrn','sheets')},
	});	}


})(jQuery);
