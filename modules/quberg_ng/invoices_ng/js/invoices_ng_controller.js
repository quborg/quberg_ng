(function($){

	Drupal.behaviors.InvoicesNg_Sidebar_Controller={ // SWITCH SIDEBAR MENU STOCK BLOCK
		attach:function(context,settings){
			var Menu={'invoice-sidebar-sheets':'#add-invoice',
								'invoices-sidebar-sheets':'#block-invoices',};
			jQuery('.invoices-menu-sidebar-link').click(function(){var Lnk=this.getAttribute("id");
				jQuery.each(Menu,function(k,v){
					if(k!==Lnk){jQuery(v).hide();jQuery('#'+k).removeClass('active-vmsl');ClearNotifications('sheets');}
					else{jQuery(v).show();jQuery('#'+k).addClass('active-vmsl');}
				});
			});
	}	};

	Drupal.behaviors.InvoicesNg_Actions={ 	// ACTIONS & BUTTONS TOOL
		attach:function(context,settings){
			jQuery('#add-new-invoice').live('click',function(){			// ADD NEW INVOICE
				jQuery('#print-sheets,#save-sheets').removeClass('disable');
				jQuery('#block-invoices').hide();jQuery('#add-invoice').show();Structure_ClearInvoiceSheet();
				jQuery('#invoices-sidebar-sheets').removeClass('active-vmsl');
				jQuery('#invoice-sidebar-sheets').addClass('active-vmsl');});
			jQuery('#close-invoice,#invoices-sidebar-sheets').live('click',function(){	 // CLOSE SHEET SHOW INVOICES
				jQuery('#print-sheets,#save-sheets').addClass('disable');
				jQuery('#add-invoice').hide();jQuery('#block-invoices').show();
				jQuery('#invoices-sidebar-sheets').addClass('active-vmsl');
				jQuery('#invoice-sidebar-sheets').removeClass('active-vmsl');});
			jQuery('#add-invoice').live('change input keyup',function(){jQuery('#save-sheets').removeClass('disable');}) 	// SHOW SAVE INVOICE BUTTON
			jQuery('#refresh-sheets').live('click',function(){Api_SheetsRefresh();});			// RELOAD INVOICES TABLE
			jQuery('#invoices-table .operation i').live('click',function(){var id=jQuery(this).attr('data-id'),op=jQuery(this).attr('data-op'),el='.invoice-line-item.'+id+' i';
				if(jQuery(this).hasClass('response')||jQuery(this).hasClass('delete'))jQuery(el).toggleClass('hide');
				if(jQuery(this).attr('data-type')=='request')jQuery(el+'.response').attr('data-action',op);
				if(jQuery(this).attr('data-op')=='confirm'&&jQuery(this).attr('data-action')=='delete')Api_InvoiceDelete(id);
				if(jQuery(this).attr('data-op')=='edit'&&jQuery(this).attr('data-type')=='request')Api_InvoiceLoadData(id);})
			jQuery('#save-sheets').live('click',function(){if(!jQuery(this).hasClass('disable')){Structure_InvoiceSaveProcess();}}) 	// SAVE SHEETS
			jQuery('#invoices-filter-action').on('click',function(){
				var n=!!jQuery('#invoice-filter-customer').attr('data-ref')?jQuery('#invoice-filter-customer').attr('data-ref'):'*',
						d=!!jQuery('#invoice-filter-date').val()?jQuery('#invoice-filter-date').val():'*',
						t=!!jQuery('#invoice-filter-type').val()?jQuery('#invoice-filter-type').val():'*',
						s=!!jQuery('#invoice-filter-status').val()?jQuery('#invoice-filter-status').val():'*';
				Api_SheetsFilters(n,d,t,s);
			})
	}	};

	Drupal.behaviors.InvoicesNg_FilterDate={ // INVOICES FILTER DATE
		attach:function(context,settings){
			if(jQuery('#invoice-filter-date').length){jQuery('#invoice-filter-date').datepicker({dateFormat:'dd/mm/yy'});}
	}	};

	Drupal.behaviors.InvoicesNg_FilterCustomer={ // INVOICES FILTER CUSTOMER
		attach:function(context,settings){window.CustomerAutoComplete=0;
			jQuery('#invoice-filter-customer').live('keyup',function(){var timer=0,value=jQuery(this).val(),orizon=jQuery('body').hasClass('i18n-ar')?'2px':'100%',el='#invoice-filter-customer',ul='#invoice-filter-customer-autocomplete';
				jQuery(this).addClass('autoc-actif'); jQuery(this).attr('data-ref','');
				if(jQuery(this).val()&&jQuery(this).val().replace(/\ /g,'')!==''){
					clearTimeout(timer);
					timer=setTimeout(function(){
						window.CustomerAutoComplete=window.CustomerAutoComplete==i++?i:++i;
						Api_CustomerAutoComplete(value,orizon,window.CustomerAutoComplete,el,ul);
					},300);
				}else{jQuery(this).removeClass('autoc-actif').css('background-position',orizon+' 4px');Structure_ClearCustomerAutocompleteList(ul);}
			});
	}	};

})(jQuery);
