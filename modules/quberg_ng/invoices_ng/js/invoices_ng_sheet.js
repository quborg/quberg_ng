(function($){

	Drupal.behaviors.InvoiceSheet_SaveInvoice={ // SAVE INVOICE
		attach:function(context,settings){
			jQuery('#save-sheets').live('click',function(){if(!jQuery(this).hasClass('disable'))Structure_InvoiceSaveProcess();})
	}	};

	Drupal.behaviors.InvoiceSheet_InvoiceTypeAndStatus={ // INVOICE TYPE & STATUS
		attach:function(context,settings){
			jQuery('#invoice-type-select').live('change',function(){
				if(jQuery('#invoice-type-select').val()==127){jQuery('#invoice-status-select').hide();jQuery('#invoice-content').addClass('quote');
				}else{jQuery('#invoice-status-select').show();jQuery('#invoice-content').removeClass('quote');}
			})
	}	};

	Drupal.behaviors.InvoiceSheet_Date={ // INVOICE DATE
		attach:function(context,settings){
			var fullDate = new Date();
			var twoDigitMonth = fullDate.getMonth()+1+"";if(twoDigitMonth.length==1)  twoDigitMonth="0" +twoDigitMonth;
			var twoDigitDate = fullDate.getDate()+"";if(twoDigitDate.length==1) twoDigitDate="0" +twoDigitDate;
			var currentDate = twoDigitDate + "/" + twoDigitMonth + "/" + fullDate.getFullYear();
			if(jQuery('#invoice-date').length){jQuery('#invoice-date').datepicker({dateFormat:'dd/mm/yy'}).val(currentDate);}
	}	};

	Drupal.behaviors.InvoiceSheet_CompanyInformation={ // INVOICE LOAD COMPANY INFORMATION
		attach:function(context,settings){
			if(jQuery('body').hasClass('front')){
				jQuery('#invoice-company-name').text(Drupal.settings.User.CompanyName);
				jQuery('#invoice-company-phone').text(Drupal.settings.User.CompanyPhone);
				jQuery('#invoice-company-adress').text(Drupal.settings.User.CompanyAdress);
			}
	}	};

	Drupal.behaviors.InvoiceSheet_Calculator_Controller={ // INVOICE CALCULATOR
		attach:function(context,settings){
			jQuery('#invoice-table,#downpayment,#vta-percent').live('input keyup change click',function(){var subtotal=0,vta=parseInt(jQuery('#vta-percent').html());
				jQuery('#invoice-table tbody tr').each(function(){var id=jQuery(this).attr('data-id');
					jQuery('#inv-tot-'+id).val(eval(jQuery('#inv-qty-'+id).val()*jQuery('#inv-pxu-'+id).val()).toFixed(2));})
				jQuery('input[id^="inv-tot-"]').each(function(){subtotal+=parseInt(jQuery(this).val())});
				jQuery('#subtotal').html(subtotal.toFixed(2));
				jQuery('#vat').html(((subtotal*vta)/100).toFixed(2));
				jQuery('#amount').html((subtotal-parseInt(jQuery('#downpayment').val())).toFixed(2));
			})
	}	};

	Drupal.behaviors.InvoiceSheet_Printer_Controller={ // INVOICE PRINTER
		attach:function(context,settings){
			jQuery("#print-sheets").live("click",function(){
				jQuery.ajax({url:Drupal.settings.Invoice.path+'/css/invoice_ng_print.css',
					success:function(data){
						var divContents=jQuery("#invoice-content").clone();
						divContents.find('#invoice-table .invoice-product-item td input').parent().each(function(){jQuery(this).html('<span>'+jQuery(this).find('input').val()+'</span>');});
						divContents.find('#customer-name-print').html(jQuery('#customer-name').val());
						divContents.find('#invoice-date-print').html(jQuery('#invoice-date').val());
						divContents.find('#downpayment-print').html(jQuery('#downpayment').val());
						var	printWindow=window.open('','','height=400,width=800');
						var	title='<title>'+jQuery('#invoice-date').val()+'_'+jQuery('.customer-name').html()+'</title>';
						divContents=divContents[0].outerHTML;
						printWindow.document.write('<html><head>'+title+'<style>'+data+'</style></head><body>'+divContents+'</body></html>');
						printWindow.document.close();
						printWindow.print();
					},
					error:function(){console.log('--');NotifyNg('wrn','sheets')},
				})
			});
	}	};

	Drupal.behaviors.InvoiceSheet_AddNewLine_RemoveLineItem={ // LINE ITEM OPERATION, ADD, REMOVE
		attach:function(context,settings){
			jQuery('#invoice-add-item').live('click',function(){
				var id=parseInt(jQuery(this).parent().parent().attr('data-id'))+1,tr='<tr id="invoice-product-item-'+id+'" class="invoice-product-item '+id+'" data-id="'+id+'"><td class="operation inv-sale-item"><i id="invoice-add-item" class="icon-reply '+id+'"></i></td><td class="inv-des" width="30%"><input id="inv-mid-'+id+'" value="'+jQuery.now()+'" type="hidden"><input type="text" id="inv-des-'+id+'" class="tocheck invoice-des form-autocomplete '+id+' autoc-actif flip-flop" autocomplete="off" data-id="'+id+'"><ul class="invoice-autocomplete '+id+' autocomplete-list"></ul></td><td width="10%"><input class="tocheck flip-flop" data-type="number" id="inv-qty-'+id+'" value="0"></td><td width="10%"><input class="tocheck flip-flop" data-type="number" id="inv-pxu-'+id+'" value="0.00"></td><td width="10%"><input type="number" id="inv-tot-'+id+'" class="tot" value="0" disabled="disabled"></td></tr>';
				jQuery('#invoice-product-item-'+(id-1)+' td.operation').html('<i id="invoice-delete-item-'+(id-1)+'" class="icon-remove22 remove-inv-sale" data-id="'+(id-1)+'"></i>');
				jQuery('#invoice-table tbody').append(tr);
				jQuery('#save-sheets').removeClass('disable');
			})
			jQuery('i[id^="invoice-delete-item-"').live('click',function(){id=jQuery(this).attr('data-id');
				if(jQuery('#invoice-id').attr('data-stk-id')!=='none'){
					qty=parseInt(jQuery('#inv-qty-'+id).val());
					ref=jQuery('#inv-des-'+id).attr('data-ref');
					row=ref+'::'+qty+':::';
					jQuery('#cbd-inv-stk-deleted').html(jQuery('#cbd-inv-stk-deleted').html()+row);
				}
				jQuery('#invoice-product-item-'+id).remove();jQuery('#save-sheets').removeClass('disable');
			})
	}	};

	Drupal.behaviors.InvoiceSheet_Autocomplete={ // AUTOCOMPLETE INVOICE SHEET PRODUCTS & CUSTOMER
		attach:function(context,settings){var timer=i=0;window.InvoiceAutoComplete=window.CustomerAutoComplete=0;
			jQuery('.invoice-des').live('keyup',function(){var value=jQuery(this).val(),id=jQuery(this).attr('data-id'),orizon=jQuery('body').hasClass('i18n-ar')?'2px':'100%';
				jQuery(this).addClass('autoc-actif');	jQuery(this).attr('data-ref',''); jQuery('#inv-pxu-'+id).val('0.00');
				if(jQuery(this).val()&&jQuery(this).val().replace(/\ /g,'')!==''){
					clearTimeout(timer);
					timer=setTimeout(function(){
						window.InvoiceAutoComplete=window.InvoiceAutoComplete==i++?i:++i;
						Api_InvoiceAutoComplete(id,value,orizon,window.InvoiceAutoComplete);
					},300);
				}else{jQuery(this).removeClass('autoc-actif').css('background-position',orizon+' 4px');Structure_ClearInvoiceAutocompleteList(id);}
			});
			jQuery('#customer-name').live('keyup',function(){var value=jQuery(this).val(),orizon=jQuery('body').hasClass('i18n-ar')?'2px':'100%',el='#customer-name',ul='#customer-autocomplete-list';
				jQuery(this).addClass('autoc-actif'); jQuery(this).attr('data-ref','');
				if(jQuery(this).val()&&jQuery(this).val().replace(/\ /g,'')!==''){
					clearTimeout(timer);
					timer=setTimeout(function(){
						window.CustomerAutoComplete=window.CustomerAutoComplete==i++?i:++i;
						Api_CustomerAutoComplete(value,orizon,window.CustomerAutoComplete,el,ul);
					},300);
				}else{jQuery(this).removeClass('autoc-actif').css('background-position',orizon+' 4px');Structure_ClearCustomerAutocompleteList(ul);}
			});
	}	};


})(jQuery);
