<?php

function views_invoices_filters(){
	return '
	<div class="filter-wrapper invoice-filter-customer"><input type="text" id="invoice-filter-customer" class="autocomplete-customers form-autocomplete" placeholder="'.t('Search in names ..').'"><ul id="invoice-filter-customer-autocomplete"></ul></div>
	<div class="filter-wrapper invoice-filter-date"><input id="invoice-filter-date" placeholder="'.t('Define a date').'"></div>
	<div class="filter-wrapper invoice-filter-type">'.render_select_invoice_type(true).'</div>
	<div class="filter-wrapper invoice-filter-status">'.render_select_invoice_status(true).'</div>';
}

function views_invoices_head(){
	return '<thead><tr><th></th><th>'.t('Date ').'</th><th>'.t('Customer').'</th><th>'.t('Amount').'</th><th>'.t('Down payment').'</th><th>'.t('Type').'</th><th>'.t('Status').'</th></tr></thead>';
}

function views_invoices_body($n,$d,$t,$s){$rows='';$datas=invoices_ng_invoices_data($n,$d,$t,$s);
	foreach($datas as$data){$invoice=$data;$data=explode('::',$data);
		$rows.='
			<tr class="invoice-line-item '.$data[0].'" data-id="'.$data[0].'" data-data="'.$invoice.'">
				<td class="operation inv-sale-item fix-width-10">
					<i data-id="'.$data[0].'" data-type="request" class="icon-minus5 delete request" data-op="delete"></i>
					<i data-id="'.$data[0].'" data-type="response" class="icon-cancel3 hide cancel response" data-op="cancel"></i>
					<i data-id="'.$data[0].'" data-type="request" class="icon-pencil edit request" data-op="edit"></i>
					<i data-id="'.$data[0].'" data-type="response" class="icon-checkmark5 hide confirm response" data-op="confirm"></i></td>
				<td class="col-dte" ><span data-id="'.$data[0].'" data-name="date">'.str_replace('-','/',$data[1]).'</span></td>
				<td class="col-nom" ><span data-id="'.$data[0].'" data-name="customer" data-ref="'.$data[2].'">'.$data[3].'</span></td>
				<td class="col-mnt" ><span data-id="'.$data[0].'" data-name="amount">'.$data[4].'</span></td>
				<td class="col-dpy" ><span data-id="'.$data[0].'" data-name="downpayment">'.$data[5].'</span></td>
				<td class="col-typ" ><span data-id="'.$data[0].'" data-name="type" data-ref="'.$data[6].'">'.$data[7].'</span></td>
				<td class="col-stu" ><span data-id="'.$data[0].'" data-name="status" data-ref="'.$data[8].'">'.$data[9].'</span></td>
			</tr>
		';
	}
	return$rows;
}

function views_invoices_new_invoice(){
	$output='
		<div id="invoice-content"><input id="invoice-id" type="hidden" value="new" data-stk-id="none">
			<input id="data-flag" type="hidden" data-type="new" data-status="new">
			<span id="close-invoice" class="icon-close2 screen"></span>
			<div class="invoice-head">
				<div class="invoice-company">
					<h2 id="invoice-company-name" contenteditable></h2>
					<div id="invoice-company-adress" class="address" contenteditable></div>
					<div id="invoice-company-phone" class="telephone" contenteditable></div>
				</div>
				<div class="invoice-customer">
					<h1 contenteditable>'.t('Invoice').'</h1>
					<div class="customer-info"><span class="customer-info-tag">'.t('for ').'</span>: <input type="text" id="customer-name" class="tocheck autocomplete-customers screen form-autocomplete"><span id="customer-name-print" class="print"></span><ul id="customer-autocomplete-list"></ul></div>
					<div class="customer-info"><span class="customer-info-tag">'.t('Date ').'</span>: <input id="invoice-date" class="tocheck screen"><span id="invoice-date-print" class="print"></span></div>
				</div>
			</div>
			<div class="invoice-body">
				<div class="invoice-table">
					<table id="invoice-table">
						<thead>
							<tr>
								<th class="invoice-sharp"></th>
								<th width="30%">'.t('Designation').'</th>
								<th width="10%">'.t('Quantity').'</th>
								<th width="10%">'.t('Unit Price').'</th>
								<th width="10%">'.t('Total').'</th>
							</tr>
						</thead>
						<tbody>
							<tr id="invoice-product-item-1" class="invoice-product-item 1" data-id="1">
								<td class="operation inv-sale-item"><i id="invoice-add-item" class="icon-reply 1"></i></td>
								<td class="inv-des" width="30%"><input id="inv-mid-1" value="'.round(microtime(true)*1000).'" type="hidden"><input type="text" id="inv-des-1" class="tocheck invoice-des form-autocomplete 1 autoc-actif flip-flop" autocomplete="off" data-id="1"><ul class="invoice-autocomplete 1 autocomplete-list"></ul></td>
								<td width="10%"><input class="tocheck flip-flop" id="inv-qty-1" value="0" data-type="number"></td>
								<td width="10%"><input class="tocheck flip-flop" id="inv-pxu-1" value="0.00" data-type="number"></td>
								<td width="10%"><input type="number" id="inv-tot-1" class="tot" value="0" disabled="disabled"></td>
							</tr>
						</tbody>
						<tfoot>
							<tr class="fake-row"><td></td><td></td><td></td><td class="bordred"></td><td class="bordred"></td></tr>
							<tr><td></td><td></td><td></td><td class="bordred"><span class="tfoot-label">'.t('Subtotal').'</span></td><td class="bordred"><span id="subtotal">0.00</span></td></tr>
							<tr><td></td><td></td><td></td><td class="bordred"><span class="tfoot-label">'.t('Downpayment').'</span></td><td class="bordred"><input id="downpayment" type="text" value="0.00" class="tocheck screen flip-flop"><span id="downpayment-print" class="print"></span></td></tr>
							<tr><td></td><td></td><td></td><td class="bordred"><span class="tfoot-label">'.t('Including VAT').'</span> <span contenteditable id="vta-percent">20</span>%</td class="bordred"><td><span id="vat">0.00</span></td></tr>
							<tr><td></td><td></td><td></td><td class="bordred"><span class="tfoot-label">'.t('Amount due').'</span></td><td class="bordred"><span id="amount">0.00</span></td></tr>
						</tfoot>
					</table>
				</div>
				<div class="invoice-alphabetic-amount">
					<div>'.t('Stop of this invoice to the sum of :').'</div>
					<div id="alphabetic-amount" contenteditable>-</div>
				</div>
			</div>
			<div class="invoice-foot">
				<div class="invoice-signature">'.t('Signature').'</div>
				<div class="invoice-comment"><textarea id="comment" pattern="^[&:]" contenteditable></textarea></div>
			</div>
		</div>';
	return $output;
}

