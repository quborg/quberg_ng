<?php

function views_contacts_table_block($type){
	$contact=array('customers'=>'Customers','suppliers'=>'Suppliers');$single=array('customers'=>'Customer','suppliers'=>'Supplier');
	$output='
		<h2 class="block-title">'.t($contact[$type]).'</h2><div id="add-new-'.$type.'" class="title-master-op"><i class="icon-addressbook"></i><span>'.t('Add '.$single[$type]).'</span></div>
		<div id="block-'.$type.'">
			<div id="block-'.$type.'-filters" class="block-sidebar-filter">
				<div class="title-wrapper">
					<h4 class="block-title">'.t($contact[$type].' filter').'</h4>
					<span id="'.$type.'-filter-action" class="contact-filter-action button" data-type="'.$type.'">'.t('search').'</span>
				</div>
				<div class="filters-wrapper">
					<div class="names-filter-wrapper">
						<input type="text" id="'.$type.'-names-filter" class="contacts-names-filter" placeholder="'.t('Search in names ..').'" data-type="'.$type.'">
						<span id="close-names-filter-'.$type.'" class="close-contacts-names-filter" data-type="'.$type.'">x</span>
					</div>
				</div>
			</div>
			<div id="block-'.$type.'-content">'.views_contacts_table($type).'</div>
		</div>';
return $output;
}

function views_contacts_table($type){$contact=array('customers'=>138,'suppliers'=>139);
	$thead='<thead><tr><th></th><th>'.t('Name').'</th><th>'.t('Company').'</th><th>'.t('Telephone').'</th></tr></thead>';
	$tbody='<tbody>'.views_contacts_body($type,'*').'</tbody>';
	$table='<table id="'.$type.'-table">'.$thead.$tbody.'</table>';
	return$table;
}

function views_contacts_body($type,$string){$contact=array('customers'=>138,'suppliers'=>139);$rows='';$datas=contacts_ng_contacts_data($contact[$type],$string);
	foreach($datas as$data){$stack=$data;$data=explode('::',$data);
		$rows.='<tr class="'.$type.'-line-item '.$data[0].'" data-id="'.$data[0].'" data-data="'.$stack.'" data-type-ref="'.$contact[$type].'" data-contact-type="'.$type.'">
			<td class="operation fix-width-10">
				<i data-id="'.$data[0].'" data-type="request" class="icon-minus5 delete request" data-op="delete"></i>
				<i data-id="'.$data[0].'" data-type="response" class="icon-cancel3 hide cancel response" data-op="cancel"></i>
				<i data-id="'.$data[0].'" data-type="request" class="icon-pencil edit request" data-op="edit"></i>
				<i data-id="'.$data[0].'" data-type="response" class="icon-checkmark5 hide confirm response" data-op="confirm"></i></td>
			<td class="fix-width-30 col-nom"><span data-id="'.$data[0].'" data-name="name">'.$data[1].'</span></td>
			<td class="fix-width-30 col-cny"><span data-id="'.$data[0].'" data-name="company">'.$data[2].'</span></td>
			<td class="fix-width-30 col-fne"><span data-id="'.$data[0].'" data-name="telephone" dir="ltr">'.$data[3].'</span></td>
		</tr>';
	}
	return$rows;
}
