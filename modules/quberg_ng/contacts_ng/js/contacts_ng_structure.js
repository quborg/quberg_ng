(function($){

	Structure_NewContacTosave=function(type){
		var tr='<tr id="new-'+type+'s-tosave" class="'+type+'s-line-item new"><td class="operation fix-width-10"><i id="remove-new-'+type+'" class="icon-cancel3 cancel"></i><i id="save-new-'+type+'" class="icon-checkmark5 confirm"></i></td><td class="fix-width-30 col-nom"><span data-name="name" contenteditable></span></td><td class="fix-width-30 col-cny"><span data-name="company" contenteditable></span></td><td class="fix-width-30 col-fne"><span data-name="telephone" dir="ltr" contenteditable></span></td></tr>';
		jQuery('#'+type+'s-table tbody').prepend(tr);}

	Structure_ContactSave=function(id,type){name='.'+type+'-line-item.'+id+' span[data-name="name"]';
		Structure_ValidateContactForm(id,type);
		if(jQuery(name).hasClass('error')){NotifyNg('err','contacts');return false;}
		Api_ContactSave(id,type);}

	Structure_ValidateContactForm=function(id,type){
		jQuery('.'+type+'-line-item.'+id+' span[data-name="name"]').removeClass('error empty');
		if(jQuery('.'+type+'-line-item.'+id+' span[data-name="name"]').text().replace(/\ /g,'')=='')jQuery('.'+type+'-line-item.'+id+' span[data-name="name"]').addClass('error empty');
		else jQuery('.'+type+'-line-item.'+id+' span').removeAttr('contenteditable');}

	Structure_SerializeContact=function(id,type){var data='id='+id,contact={'customers':138,'suppliers':139};
		data+='&name='+jQuery('.'+type+'-line-item.'+id+' span[data-name="name"]').text();
		data+='&company='+jQuery('.'+type+'-line-item.'+id+' span[data-name="company"]').text();
		data+='&telephone='+jQuery('.'+type+'-line-item.'+id+' span[data-name="telephone"]').text();
		data+='&type='+contact[type];
		return data;}

	Structure_RebuildContactRow=function(id,type){	console.log(type+' Rebuild :'+id);
		var types={'customers':138,'suppliers':139},
				nom=jQuery('.'+type+'-line-item.'+id+' span[data-name="name"]').text(),
				cny=jQuery('.'+type+'-line-item.'+id+' span[data-name="company"]').text(),
				fne=jQuery('.'+type+'-line-item.'+id+' span[data-name="telephone"]').text(),
				data=id+'::'+nom+'::'+cny+'::'+fne;
				console.log(data);
		jQuery('.'+type+'-line-item.'+id).attr('data-data',data);
		jQuery('#new-'+type+'-tosave').attr({'id':'','class':type+'-line-item '+id,'data-id':id,'data-data':data,'data-type-ref':types.type,'data-contact-type':type});
		jQuery('.'+type+'-line-item.'+id+' td.operation').html('<i data-id="'+id+'" data-type="request" class="icon-minus5 delete request" data-op="delete"></i><i data-id="'+id+'" data-type="response" class="icon-cancel3 hide cancel response" data-op="cancel"></i><i data-id="'+id+'" data-type="request" class="icon-pencil edit request" data-op="edit"></i><i data-id="'+id+'" data-type="response" class="icon-checkmark5 hide confirm response" data-op="confirm"></i>');
		jQuery('.'+type+'-line-item.'+id+' td span').attr('data-id',id); }


})(jQuery);
