(function($){

	types={138:'customers',139:'suppliers'};

	Api_ContactSave=function(id,type){var data=Structure_SerializeContact(id,type);
		jQuery.ajax({url:'contacts_ng/save/'+id,type:'POST',data:data,
			beforeSend:function(){jQuery('#wait-action-load').addClass('show');},
			success:function(data){if(parseInt(jQuery(data).filter('contactid').text())){NotifyNg('stu','contacts');Api_cbd_LoadData_CustomersNames(type);Structure_RebuildContactRow(jQuery(data).filter('contactid').text(),type);}else{console.log('--');NotifyNg('wrn','contacts');}},
			complete:function(){jQuery('#wait-action-load').removeClass('show');},
			error:function(){console.log('--');NotifyNg('wrn','contacts')},
		});
	}

	Api_ContactsNamesFilter=function(type,string){
		jQuery.ajax({url:'contacts_ng/contacts_rows/'+type+'/'+string,
			beforeSend:function(){jQuery('#wait-action-load').addClass('show');},
			success:function(data){
				console.log(data);
				jQuery('#'+type+'-table tbody').html(data);
			},
			complete:function(){jQuery('#wait-action-load').removeClass('show');},
			error:function(){console.log('--');NotifyNg('wrn','contacts')},
		});
	}

	Api_ContactsRefresh=function(type){
		jQuery.ajax({url:'contacts_ng/contacts_rows/'+type+'/*',
			beforeSend:function(){jQuery('#wait-action-load').addClass('show');},
			success:function(data){jQuery('#'+type+'-table tbody').html(data);jQuery('#close-names-filter').trigger('click')},
			complete:function(){jQuery('#wait-action-load').removeClass('show');},
			error:function(){console.log('--');NotifyNg('wrn','contacts')},
		});
	}

	Api_DeleteContact=function(id,type){
		jQuery.ajax({url:'contacts_ng/contact_delete/'+id,
			beforeSend:function(){jQuery('#wait-action-load').addClass('show');},
			success:function(data){if(jQuery(data).filter('span').text()=='success'){jQuery('.'+type+'-line-item.'+id).remove();NotifyNg('stu','contacts');Api_cbd_LoadData_CustomersNames();}
				if(jQuery(data).filter('span').text()=='not found')NotifyNg('err','contacts');},
			complete:function(){jQuery('#wait-action-load').removeClass('show');},
			error:function(){console.log('--');NotifyNg('wrn','contacts')},
		});
	}

	Api_cbd_LoadData_CustomersNames=function(){console.log('load customers');
		// jQuery.ajax({url:'contacts_ng/customers_names',
		// 	success:function(data){if(data!=='wrn')jQuery('#content-block-data #cbd-customers-names').html(data);},
		// 	error:function(){console.log('--');},});
	}


})(jQuery);
