(function($){

	Drupal.behaviors.ContactsNg_Sidebar_Controller={ // SWITCH SIDEBAR MENU BLOCK
		attach:function(context,settings){
			var Menu={'customers-sidebar-contacts':'#customers-block',
						 'suppliers-sidebar-contacts':'#suppliers-block',};
			jQuery('.contacts-menu-sidebar-link').click(function(){var Lnk=this.getAttribute("id");
				jQuery.each(Menu,function(k,v){
					if(k!==Lnk){jQuery(v).hide();jQuery('#'+k).removeClass('active-vmsl');ClearNotifications('sheets');}
					else{jQuery(v).show();jQuery('#'+k).addClass('active-vmsl');}
				});
			});
	}	};

	Drupal.behaviors.ContactsNg_ActionsTool_Controller={ // ACTIONS & BUTTONS TOOL
		attach:function(context,settings){
			jQuery('#add-new-customers').live('click',function(){if(!jQuery('#new-customer-tosave').length)Structure_NewContacTosave('customer');});
			jQuery('#remove-new-customer').live('click',function(){jQuery('#customers-table tr#new-customers-tosave').remove();});
			jQuery('#add-new-suppliers').live('click',function(){if(!jQuery('#new-supplier-tosave').length)Structure_NewContacTosave('supplier');});
			jQuery('#remove-new-supplier').live('click',function(){jQuery('#suppliers-table tr#new-supplier-tosave').remove();});
			jQuery('#refresh-contacts').live('click',function(){if(jQuery('#block-customers').css('display')=='block')Api_ContactsRefresh('customers');if(jQuery('#block-suppliers').css('display')=='block')Api_ContactsRefresh('suppliers');});
			jQuery('#save-new-customer').live('click',function(){Structure_ContactSave('new','customers');});
			jQuery('#save-new-supplier').live('click',function(){Structure_ContactSave('new','suppliers');});
			jQuery('#block-contacts-ng-contacts-ng .operation i').live('click',function(){
				var id=jQuery(this).attr('data-id'),
						op=jQuery(this).attr('data-op'),
						type=jQuery(this).parent().parent().attr('data-contact-type');
						el='.'+type+'-line-item.'+id+' i';
				jQuery(el).toggleClass('hide');
				if(jQuery(this).attr('data-type')=='request')jQuery(el+'.response').attr('data-action',op);
				if(jQuery(this).attr('data-op')=='confirm'&&jQuery(this).attr('data-action')=='delete')Api_DeleteContact(id,type);
				if(jQuery(this).attr('data-op')=='edit')jQuery('.'+type+'-line-item.'+id+' span').attr('contenteditable','');
				if(jQuery(this).attr('data-op')=='cancel'&&jQuery(this).attr('data-action')=='edit'){var ram=jQuery('.'+type+'-line-item.'+id).attr('data-data').split('::');
					jQuery('.'+type+'-line-item.'+id+' span').removeAttr('contenteditable').each(function(){
						if(jQuery(this).attr('data-name')=='name')jQuery(this).html(ram[1]);
						if(jQuery(this).attr('data-name')=='company')jQuery(this).html(ram[2]);
						if(jQuery(this).attr('data-name')=='telephone')jQuery(this).html(ram[3]);})}
				if(jQuery(this).attr('data-op')=='confirm'&&jQuery(this).attr('data-action')=='edit'){
					if(jQuery('#block-'+type).css('display')=='block')Structure_ContactSave(id,type);};});
			jQuery('.contact-filter-action').live('click',function(){var type=jQuery(this).attr('data-type'),value=jQuery('#'+type+'-names-filter').val();
				if(value.replace(/\ /g,'')!==''){
					Api_ContactsNamesFilter(type,value);
					jQuery('#'+type+'-names-filter').attr('data-search','true');
				}else Api_ContactsRefresh(type);});
			jQuery('.contacts-names-filter').on('input keypress',function(){var value=jQuery(this).val(),type=jQuery(this).attr('data-type');
				if(value.replace(/\ /g,'')!=='')jQuery('#close-names-filter-'+type).show();
				else jQuery('#close-names-filter-'+type).hide();})
			jQuery('.close-contacts-names-filter').on('click',function(){var type=jQuery(this).attr('data-type');
				jQuery('#'+type+'-names-filter').val('').trigger('input');
				if(jQuery('#'+type+'-names-filter').attr('data-search')=='true'){
					jQuery('#'+type+'-names-filter').attr('data-search','');
					Api_ContactsRefresh(type);}})
	}	};


})(jQuery);
